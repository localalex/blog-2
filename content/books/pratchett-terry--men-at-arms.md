---
title: "Terry Pratchett - Men at Arms"
date: 2020-08-09
started: 2020-07-20
goodreads: https://www.goodreads.com/book/show/833425.Men_at_Arms
shelves:
  - fiction
  - fantasy
  - comedy
---

After reading [Guards! Guards!](/books/pratchett-terry-guards-guards/), I was hooked to the Watch series and this book was no disappointment. Pratchett develops the peculiar characters of the Ankh-Morpork city night watch (Nobby, Carrot) even further and adds new watch members: a dwarf, a troll (I love Detritus), and an undead.

I've listened to this book as an audiobook and people had to think I'm crazy when walking through the city and laughing out loud most of the time. This book is golden, especially when read by Nigel Planer.

Next up: [Feet of Clay](https://www.goodreads.com/book/show/833426.Feet_of_Clay)

[![Discworld reading order](https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Discworld_Reading_Order_Guide_3.0_%28cropped%29.jpg/780px-Discworld_Reading_Order_Guide_3.0_%28cropped%29.jpg)](https://en.wikipedia.org/wiki/Discworld#/media/File:Discworld_Reading_Order_Guide_3.0_(cropped).jpg)

Source: Wikipedia
