---
title: "Karel Čapek - R.U.R."
date: 2020-01-20
started: 2020-01-16
goodreads: https://www.goodreads.com/book/show/436562.R_U_R_
shelves:
  - fiction
  - sci-fi
---

I've picked this book for two reasons. It's my country's classics and it's super short. The 3 hours that it took to read R.U.R. was time well spent. If the book was any longer, I'd mind the old and rudimentary style (I guess inherent to plays).

Many of the concepts mentioned in this play are still valid today, e.g. Should we allow AI to make AI? Should robots carry guns? Can people behave cruelly to a robot without consciousness? And is it ok to replace human work force with machine learning?

That being said the early 20th century style comes at a cost of for example having to endure the role of Helena whose naivety and silliness and the whole overall subtext of (women are supposed to be pretty and not much else) pisses me off.
