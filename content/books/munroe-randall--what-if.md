---
title: "Randall Munroe - What if?: serious scientific answers to absurd hypothetical questions"
date: 2021-01-29
started: 2021-01-05
openlibrary: https://openlibrary.org/books/OL26327044M/What_if
shelves:
  - non-fiction
  - science
  - comedy
---

Same as in [How to](/books/munroe-randall--how-to/), it's a pleasure to see Randall's thought process when answering absurd but interesting physics questions. The following snippet from the book shows this well:

> A mole (the animal) is small enough for me to pick up and throw.[citation needed] Anything I can throw weighs 1 pound. One pound is 1 kilogram. The number 602,214,129,000,000,000,000,000 looks about twice as long as a trillion, which means it's about a trillion trillion. I happen to remember that a trillion trillion kilograms is how much a planet weighs.

In this spirit, you'll learn what would happen if you: build a wall looking like the periodic table where each brick is made out of the corresponding element, use a machine gun as a jetpack, lost all your DNA in a split second.

The book both entertained me and thought me more about physics. Randall's books are the perfect material from which we can teach children that physics is fun.
