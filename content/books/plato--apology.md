---
title: "Plato - Apology"
date: 2021-11-21
started: 2021-11-21
goodreads: https://www.goodreads.com/book/show/73945.Apology
shelves:
  - non-fiction
  - philosophy
---

Apology is Plato's book with the best Goodreads rating. Plato published it in 399 BC. I picked it up because I'm excited about learning something this timeless.

The book is short, roughly 100 pages, and it describes Socrates' defence before Athenians sentenced him to death for corrupting the youth and not believing in goods.

The defence wasn't particularly good by today's standards. The arguments Socrates used were not convincing. On the other hand, 2400 ago, this must have been the top-notch argumentation. And to be fair, I have no idea how would I defend myself for not believing in gods.

The defence logic didn't impress me, but the morality did. Socrates had his values and standards, and he died rather than sacrificing those values. He left three sons behind.

I recommend reading the book because it's a classic and will only take a couple of hours. The language is old, and you'll need a dictionary at hand. I read the version with foreword and translation from James J. Helm.
