---
title: Pierce Brown - Red Rising
date: 2020-08-23
started: 2020-08-17
goodreads: https://www.goodreads.com/book/show/15839976-red-rising
shelves:
  - fiction
  - sci-fi
---

Trilogy: Red Rising, [Golden Son](/books/pierce-brown-golden-son), [Morning Star](/books/pierce-brown-morning-star)

Red rising is what happens when you cross-bread [Brave New World](https://www.goodreads.com/book/show/5129.Brave_New_World) with [The Maze Runner](https://www.goodreads.com/book/show/6186357-the-maze-runner) and the [Game of Thrones](https://www.goodreads.com/book/show/13496.A_Game_of_Thrones). Even though the book's concepts are not original, the way how Pierce Brown puts them together is incredibly amusing and propelled me into a two-week binge read of the whole trilogy.

In this science-fiction dystopian novel, the main hero is a kid from the lowest cast in the solar system. He's about to stir up an uprising for a better world and for democracy which happens to be a dirty word. The whole story happens on Mars, and instead of saying more, I recommend you get the book and read it. But be warned, it's similar to Game of Thrones addictiveness.
