---
title: "Terry Pratchett - Jingo"
date: 2020-10-30
started: 2020-10-10
goodreads: https://www.goodreads.com/book/show/833436.Jingo
shelves:
  - fiction
  - fantasy
  - comedy
---

The disadvantage of listening to the Discworld books in the form of audiobooks is that the uncontrollable bursts of laughter make you look quite silly. Jingo is a story of nationalism, war and murder. But it's not a grim book that would bring you down when you read it. Even though the underlying topics are serious, the form couldn't be more entertaining. You, for example, realise how little control do ordinary people have over their country's policy. But you also hear the racial stereotypes from the mouths of Nobby Nobbs and Sergeant Colon which takes any seriousness away.

The Klatchian D'regs are a great new addition to the Discworld. No-nonsense warriors who always charge at dawn and generally have the reputation of very egalitarian culture, they expect their woman, children and camels to fight well.
