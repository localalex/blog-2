---
title: "Frank Herbert - Dune"
date: 2021-10-03
started: 2021-09-06
openlibrary: https://openlibrary.org/works/OL893415W/Dune?edition=ia%3Aduneherb00herb
shelves:
  - fiction
  - sci-fi
---

I've done it again. I've read the Dune for the third time. Actually, this time I listened to the controversial 2007 Audio Renaissance audiobook. Controversial because it's dramatized with music and read by 12 different voice actors. I liked the format because it was easier to make sense of the many characters in the book.

Herbert wrote Dune in a way that avoids specifics of how the technology looks and works, and so it's as fun and "current" in 2021 as it was in 1965 when it was first published.

The audiobook is a bit extreme with its ** twenty-one hours**. But I listened to it while driving across Europe, so listening to Dune filled time that I would otherwise listen to something regardless.

## Why should you read it?

Because the new movie is out! And you've got a unique opportunity to use your imagination to create Arakis world in your head before Villeneuve draws it for you in his 2021 Dune movie.
