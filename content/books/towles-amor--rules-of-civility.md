---
title: "Amor Towles - Rules of Civility"
date: 2020-02-23
started: 2020-02-02
goodreads: https://www.goodreads.com/book/show/10054335-rules-of-civility
shelves:
  - fiction
  - history
---

Rules of Civility made me love the idea New York even though I've never been. Following the story of 22yo Katherine Kontent I've found her the most relatable female character ever (maybe because the author is a guy?). And her dilemma when she had to often decide between love, friendship and career was unbelievably relatable to me.

The story in this book is a story of life choices and how they can change your life for good or bad in an instant. It's capturing the essence of one's twenties: a lot of friends, a lot of choices made, some good some bad. But don't be afraid, even though this is not a cookie cutter happy, upbeat story, you won't fall into depression after reading it.

This novel has been great for my vocabulary too. Even though there was a tremendous amount of new words I could almost always guess their meaning in the context, saving me the round trip to the dictionary.
