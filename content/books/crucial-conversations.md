---
title: "Crucial Conversations"
date: 2021-10-14
started: 2021-10-03
openlibrary: https://openlibrary.org/works/OL282391W/Crucial_Conversations
shelves:
  - non-fiction
  - self-help
---
The four authors who wrote this book also started a consultancy to help resolve conflicts and increase efficiency in communication. The book is a fantastic resource for having a crucial conversation. The authors define a *crucial conversation* as a conversation where:

- stakes are high
- emotions run wild
- opinions differ

Ever had one of those? ;)

## Why should you read it?

You should read Crucial Conversations because you'll get a good framework for dealing with conflict in a productive way. It will serve you well in both your professional and personal life. The book does not offer tricks on how to "win" over the other person. It strives to create win-win situations, helping people understand each other rather than impose their opinions on others.

## Framework in a nutshell

The book has roughly 200 pages, and you can read it in an afternoon. I encourage you to read it because I won't be able to do the method justice.

There are several key concepts in the method:

### Shared pool of meaning

A shared pool of meaning is the shared understanding of the group. If people share their meaning (understanding) it becomes public for the group. Everyone sees how the other person reached their conclusion, and when the group makes a decision, they can consider everyone's understanding, possibly getting a better result.

### Dialogue

They define *dialogue* as a free flow of meaning between people. It means filling the *shared pool of meaning*. As soon as someone starts feeling unsafe, the flow is no longer free, and people either withdraw from the conversation (flight) or start forcing their meaning into the pool (fight).

### Safety

Feeling safe is the primary condition for the dialogue to happen. You can build safety on mutual respect and a common goal. If you don't respect the other person (you think they are dumb, for example) or don't share the same goal (you both want the same piece of pie), then there is a breach of safety, and the dialogue starts breaking down.

### Stories

You interpret what you see and hear and attach a meaning to it. The attached meaning is a story. The book is very adamant about the difference between facts (what you see and hear) and the story (how you interpret the facts).

In my opinion, learning to distinguish between **fact** (he looked at his phone a lot) and a **story** (he's disrespectful and doesn't care about me) alone would **make the book worth reading**. These two are so easy to confuse, and when you read a few examples, you'll start noticing how often you do that in your relationships.

## What should you be careful about?

The book often reads like an infomercial for the method and the consultancy itself. It is understandable since their livelihood depends on it, but when you read the book, you need to stay critical of some of the statements because their authors also conducted case studies that back them up.

However, the book's concepts match my understanding of how the brain works (or fails to work), and I'm confident that the method makes sense.
