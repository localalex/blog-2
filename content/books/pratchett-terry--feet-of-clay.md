---
title: "Terry Pratchett - Feet of Clay"
date: 2020-10-09
started: 2020-09-27
goodreads: https://www.goodreads.com/book/show/833426.Feet_of_Clay
shelves:
  - fiction
  - fantasy
  - comedy
---

Who can mix crime novel with thoughtful discrimination critique and make it hilarious enough that you are going to be bursting out laughing? Only Sir Terrance.

The main characters of the city watch are fully developed by now. You hear the name like Carrot or Nobby, and you start giggling without even hearing the rest.

The most entertaining part of this story was Nobby trying to avoid his faith as a nobleman. He's got an excellent spider-sense when it comes to a free lunch, and we could all learn from him.
