---
title: "Terry Pratchett - The Fifth Elephant"
date: 2020-12-04
started: 2020-11-02
openlibrary: https://openlibrary.org/books/OL7815225M/The_fifth_elephant
shelves:
  - fiction
  - fantasy
  - comedy
---

This book takes the watch outside of Ankh-Morkork. I'm always surprised how fresh, non-repetitive and unpredictable the story is even though this is the fifth book on the same theme. Kudos to Terry.

Uberwald, the wild west of Discworld is an excellent place for city-dwelling characters to step out of their comfort zone. **Next sentence contains a mild spoiler**. This is also the first book of the series where I felt that the story got darker, and one of the characters I liked dies.
