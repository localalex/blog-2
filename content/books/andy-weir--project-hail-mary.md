---
title: "Andy Weir - Project Hail Mary"
date: 2021-10-30
started: 2021-10-22
openlibrary: https://openlibrary.org/works/OL24554940W/Project_Hail_Mary
shelves:
  - fiction
  - sci-fi
---
I am genuinely excited about this book. It made me do two things I hadn't done in years: dream about the book, and interrupt my reading because the story got too intense.

I've never read Martian, Andy's previous book, so I had no expectations when opening this book. I learned about the book from someone praising it on our company Slack. The storyline sucked me in and spit me out a week later when I finished listening to the captivating story.

## Why should you read it?

You should read this book if you enjoy sci-fi and science. The story is happening in the present, and all the technology sounds plausible. It's fascinating to think that this story could happen if only we got the fuel to power our spaceship.

This book is very similar to Bobiverse series from Dennis E. Taylor. It describes a more advanced technology but not so advanced that you can't imagine the current science progressing that far.

If you want to read a story that explores nearby solar systems, if you want to read about space travel that could almost happen today, and if you want to get entertained with a very eventful storyline, Project Hail Mary is your next book!
