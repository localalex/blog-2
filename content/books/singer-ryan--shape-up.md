---
title: Ryan Singer - Shape Up
fullTitle: "Shape Up: Stop Running in Circles and Ship Work that Matters" 
date: 2020-08-14
started: 2020-07-30
goodreads: https://www.goodreads.com/book/show/50776459-shape-up
shelves:
  - non-fiction
  - business
---

This book is free to read or download on the [Basecamp website](https://basecamp.com/shapeup).

This short book (130 pages) is a great overview of the product development process used by Basecamp. As it is always the case with books from this company, there is plenty of controversial or counter-intuitive thoughts as well as plain genius ones.

## Budgeting (Appetite)

Budgeting is a method of managing software development when the team first introduces a limit on the maximum amount of resources (time, people) spent on the project. The team decides that solving the problem is only worth X amount of resources. If the project doesn't fit in that budget, it's not worth doing. At Basecamp, they call this Appetite.

## Decentralized lists

This is an outrageous idea. At Basecamp, they don't have one central backlog of all the issues or feature ideas. They say that grooming this backlog is not worth the effort because most of the tasks are never going to get done. Instead, each department in Basecamp has its list of "issues we'd like to do", and they try to shape them into projects. The argument is that the important issues are going to come up over and over again. I guess this can work for a small company of 50 people.

## Hill charts

Hill charts are an ingenious way of displaying the project progress. Instead of trying to put estimates on the unfinished parts of the project, they visually display the level of uncertainty on a "normal distribution" hill chart. I recommend reading the [Work is like a hill](https://basecamp.com/shapeup/3.4-chapter-13#work-is-like-a-hill) chapter in full.
