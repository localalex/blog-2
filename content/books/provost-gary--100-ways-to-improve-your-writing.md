---
title: Gary Provost - 100 Ways to Improve Your Writing
date: 2020-06-19
started: 2020-06-14
goodreads: https://www.goodreads.com/book/show/41769546-100-ways-to-improve-your-writing-updated
shelves:
  - non-fiction
  - writing
---

One of Gary's tips is "Make yourself likeable". And does he make a good job of it. The book describes the rules of both style and grammar in a way that's easy to digest and down to earth. A much larger portion of the book is spent on style-related advice. That's a good thing when tools like Grammarly and Hemingway app provide advanced grammar and composition checks.

The key takeaway from the book is the word **Simplicity**. Don't write something in two words when one will do the job. Don't add a sentence that doesn't contribute to the topic. Cut away sentences from the start and from the end, till you can't cut any further. That's how you find your text's real beginning and end.
