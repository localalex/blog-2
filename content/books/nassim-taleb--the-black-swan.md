---
title: "Nassim Taleb - The Black Swan"
date: 2021-11-10
started: 2021-08-09
openlibrary: https://openlibrary.org/works/OL3295030W/The_Black_Swan
shelves:
  - non-fiction
  - finance
  - rationality
---

The Black Swan is a name for an event that is completely unpredictable. The original example is discovering black swans in Australia, disproving the observation that all swans are white.

Nassim explains the problem he sees with measuring probability and how people use estimates to guide their important decisions.

## Why shouldn't you read it?

Unconventionally, I'll start by telling you why should you **not** read the book. Nassim is an insufferable author. The book has 366 pages, but it could be reduced to 150 if Nassim left out his ego and narcissism. The book could have been a pleasant, enlightening, and *short* read if it wasn't for the insults, tangents, and unnecessarily complicated words.

Nassim is intelligent and unconventional in his ideas, but he also chose this book to settle petty disputes he encountered in his life. Overall, you'll have to take a lot of *punishment* to extract the good ideas. You can read a few well-rated articles on [LessWrong](https://www.lesswrong.com/) and end up better off.

## Why should you read it?

The ideas in the book are interesting. I'm still new to the rationality movement, and the book gave me valuable insights into thinking about uncertainty and work scalability. If you've got plenty of time and patience and you can tolerate all the things I warned you about, give the book a shot.

## Key takeaways

There are **two types of randomness**. Mild and extreme. The mild randomness[^1] applies when there's a natural limit to the variable, such as human height. The extreme randomness[^2] applies when there's no natural limit, like a person's net worth. The key point in the book is that black swans happen when we mistake extreme randomness for a mild one. For example, when we assume that market movements are governed by mild randomness.

There is **scalable vs non-scalable work.** When your work doesn't scale, that means that your reward is a function of how much time and effort you put in. Lawyers, doctors and engineers are in this category. If your work scales, then there is no limit on your reward. Startup founders, investors and book authors are doing scalable work. The problem with scalable work is that very few winners take most of the rewards.

**Nobody can predict the future.** It's useless to listen to experts when they predict the future. There's plenty of data to support this in financial markets. [Check the 2021 S&500 predictions](https://compoundadvisors.com/2021/how-to-invest-without-knowing-the-future).

**More supporting evidence doesn't have much value.** After the first few values, any further evidence supporting your hypothesis doesn't increase your confidence in the hypothesis. It can still be wrong. Two examples are:

- Newtonian physics (there was a large body of supporting evidence till Einstein proved it incomplete)
- Imaginary turkey (the bird) who thinks that people have its best interest in mind because they are feeding it (each day the confidence increases until Thanksgiving).

[^1]: He calls it Gaussian randomness.
[^2]: He calls it Mandlebrotian randomness.
