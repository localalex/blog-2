---
title: "Sönke Ahrens - How to Take Smart Notes: One Simple Technique to Boost Writing, Learning and Thinking – for Students, Academics and Nonfiction Book Writers"
date: 2020-06-06
started: 2019-06-02
goodreads: https://www.goodreads.com/book/show/34507927-how-to-take-smart-notes
shelves:
  - non-fiction
  - note taking
---

This book is a good introduction into the motivation and science behind using atomic interconnected notes, a method known as slip-box or German Zettelkasten. The massive drawback of the book is that the description of the system is not giving enough examples of the process. You are therefore not able, for example, to make slip-box notes directly from the book without looking into external resources like the [zettelkasten.de](https://zettelkasten.de/) site.

I liked how succinctly author explained how important it is to have a feedback process in your learning and how slip-box process gives this feedback in several different forms:

- making notes from literature in your own words validates your understanding of the text
- turning your literature notes into permanent notes gives you feedback about how the literature fits into your current understanding of the topic.
