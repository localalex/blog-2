---
title: "Richard P. Feynman - What Do You Care What Other People Think?"
date: 2020-05-13
started: 2020-05-04
goodreads: https://www.goodreads.com/book/show/35167718-what-do-you-care-what-other-people-think
shelves:
  - non-fiction
  - biography
  - science
---

Richard Feynman seems like a really cool guy. I've never heard of him and I still don't know what he got his Nobel Prize for, but the way how he discusses his childhood, love for science and loved one's shows that he had both his brain and his hearth in the right place.

I enjoyed the part about his childhood the most, because it was not just fun, but there were lessons about parenting that I can learn from Richard's mother. The main ones being:

- Never punish a child for its curiosity.
- Explain kid the concepts, reward understanding, not memorization.

The final address speech for the academy of sciences brought home one important point. We are all ignorant, nothing is certain, and we should be careful about assuming we are right.
