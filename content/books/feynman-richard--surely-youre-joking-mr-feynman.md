---
title: "Richard P. Feynman - Surely You're Joking Mr Feynman!"
date: 2021-08-01
started: 2021-09-18
openlibrary: https://en.wikipedia.org/wiki/Surely_You%27re_Joking,_Mr._Feynman!
shelves:
  - non-fiction
  - biography
  - science
---

This book was published in 1985 as a loose collection of stories from [Feynman](https://en.wikipedia.org/wiki/Richard_Feynman) 's life. If he was at least half as gifted/driven as he makes himself to be in this book, he had to be an amazing person to be around. The book is hilarious and I laughed out loud many times.

## Why should you read it?

- Read it if you are interested in reasoning from first principles. - This book is very often quoted by people interested in critical thinking and reasoning from first principles. In the book, Feynman often emphasizes not believing what other people say automatically and thinking about the topic independently.
- Read it if you'd like to see how living by the rules of the scientific method looks like. - Feynman questioned everything and anyone. In his last chapter, "Cargo cult science", he succinctly summed up what is wrong with modern science (motivated reasoning, not enough emphasis on reproducibility). His commentary is still true today, even though he wrote it in the 70s.

## What to be careful about?

- Even though undoubtedly a brilliant and accomplished scientist, Feynman seems to be quite full of himself. You can't read the book as an objective account of Feynman's life but instead, read it as if Feynman told you the stories over a beer in a pub. Take it with a grain of salt.
- Parts of the book are getting very technical. You might have to re-read some pages multiple times (or skip them).
