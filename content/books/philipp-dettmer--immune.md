---
title: "Philipp Dettmer - Immune"
date: 2021-12-21
started: 2021-11-18
openlibrary: https://openlibrary.org/works/OL24410778W/Immune
shelves:
    - non-fiction
    - biology
    - science
---

Immune is a popular-science book explaining the human immune system. It presents a complex topic in layman's language.

Philipp Dettmer, the book's author, is the creator of the educational YouTube channel [Kurzgesagt - in a nutshell](https://www.youtube.com/c/inanutshell). I've been watching Kruzgesagt for many years, and I always appreciated their clear and objective messaging. A few months ago, the channel mentioned the book, and that was that.

You don't need to know anything about biology to read it. I haven't studied biology past the primary-school introduction, and I could still understand the concepts. The fact that I could understand this book is a testimony to the clarity of the author's explanation.

When I need to explain a complex computer system to a colleague, I use a scenario that touches all the important parts of the system. For example, "Person searches on the page for properties for sale" or "Person wants to comment on a code review". Using a scenario puts the important system's components into perspective so my colleague can remember the walk-through better.

Philipp chose the same approach with the immune system. He picked two infection scenarios that give the reader a holistic understanding of how our immune system works. These scenarios are:

-   Stepping on a rusty nail (explains bacterial infection)
-   Catching flu (explains viral infection)

The book wasn't perfect, sometimes it dumbed down the explanation too much, and sometimes there were logical inconsistencies that made it impossible to grasp some details. But overall, I believe that reading this book is your best chance at getting a basic understanding of the immune system. The book is 370 pages, and you can read it as a novel, and you'll rarely go back and re-read a paragraph or a page.
