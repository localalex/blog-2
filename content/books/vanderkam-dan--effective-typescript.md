---
title: "Dan Vanderkam - Effective TypeScript"
date: 2021-03-05
started: 2021-02-15
openlibrary: https://openlibrary.org/books/OL29447946M/Effective_TypeScript
shelves:
  - non-fiction
  - software engineering
---

When I started my programming career as a Java developer, my boss recommended that I read Effective Java. Even though I had a few semesters of Java programming behind my belt, I had a very superficial knowledge of the language till I read Effective Java.

Now I have a similar experience with Vanderkam's book. I've been programming in TypeScript for half a year, but I still didn't understand many language concepts. After reading the book, I can debug type errors quickly, and I can write code with fewer type annotations, relying heavily on type inference.

The book contains sixty-two short sections called "items". You can read a couple a day, and you'll still finish it in a month. It's also good to read only a few items per day, so the concepts can sink in and exercise them when you are programming.
