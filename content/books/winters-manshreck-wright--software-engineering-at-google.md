---
title: "Software Engineering at Google"
date: 2021-05-22
started: 2021-02-05
openlibrary: https://openlibrary.org/works/OL22293580W/Software_Engineering_at_Google
shelves:
  - non-fiction
  - software engineering
---

Subtitle: "Lessons Learned from Programming Over Time".

This book is fantastic. It goes over all software engineering disciplines and touches on testing, documentation, CI, deployment and much more.

It is similar to the Pragmatic Programmer because it focuses on general engineering advice. But it is less concerned with individuals and more with the teams and maintainability of software at scale. I appreciated the number of anecdotes (case studies), which made some of the advice more concrete and actionable.

It took me three and a half months to read this book. I made roughly 40 permanent notes to my [Zettlekasten notes](/posts/slip-box). Even though most of the concepts weren't foreign to me, the authors (Titus Winters, Tom Manshreck, Hyrum Wright) described them much clearer, and I understood them better after reading this book.

Reading this book gives an overview of engineering on a relatively high level without too many code examples. If you expect to improve your programming, I recommend books like Clean code and Refactoring. This book will teach you better engineering practices, not better programming.
