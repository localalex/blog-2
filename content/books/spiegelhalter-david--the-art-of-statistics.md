---
title: "David Spiegelhalter - The Art of Statistics: How to Learn from Data"
date: 2020-02-12
started: 2020-01-07
goodreads: https://www.goodreads.com/book/show/43722897-the-art-of-statistics
shelves:
  - non-fiction
  - science
---

This book is something. A chapter in I went to a nearby office supplies store and I bought notebook to make notes. The first few chapters I felt like statistician. Sir David Spiegelhalter (yeah, you can get knighted for statistics) explained the basics of statistics really clearly and it gave me hope that even I could get statistically enlightened even though I always struggled with the concepts. This was the 5* part.

Then past the regression modelling the book transformed into hard to understand mumbo-jumbo and I was able to get to the closing chapters just by the unprecedented power of will. This was the "Screw this, I'm going to quit" 2* part

The last 3 chapters were again written for humans with some good takeaways, overall 4* mainly because David is a Knight.
