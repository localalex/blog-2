---
title: "George S. Clason - The Richest Man in Babylon"
date: 2021-04-10
started: 2021-04-06
openlibrary: https://openlibrary.org/books/OL24088792M/The_Richest_Man_in_Babylon
shelves:
  - non-fiction
  - finance
---

This short book is a good introduction to the world of personal finance. First released in 1926, it predates modern stock markets and cryptocurrencies, but its truths are no less valid.

The book contains several stories placed in ancient Babylon. Each is presenting one or more morals related to saving money. I listened to the audiobook, which lasted four hours, just an hour longer than a Joe Rogan podcast.

The book's personal finance advice is solid and worth the read, but I wonder whether the "biblical" language used throughout the book can deter a young audience. If I haven't had an experience with investing, it would be easy to ignore the book's advice as something that's hundreds of years old and without consequence in the modern world.

> Surely thou canst get hold of a few coppers and a piece of silver to repay the generosity of an old friend of thy father who aided thee whenst thou wast in need?

Overall, the book was fun to listen to. It was a bit one-sided: all the characters and narrator cared about was gold, but if you write a book with under a hundred pages, there's no space for nuanced life advice.
