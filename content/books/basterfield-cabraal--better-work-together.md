---
title: "Susan Basterfield, Anthony Cabraal - Better work together: How the power of community can transform your business"
date: 2020-01-05
started: 2019-11-13
goodreads: https://www.goodreads.com/book/show/42737756-better-work-together
shelves:
  - non-fiction
---

The book follows a story of an entity called Enspiral from New Zealand (if this sounds vague that's because it is). After reading the book you find out that Enspiral started as a consulting company but evolved as a very egalitarian community of people.

The book is interesting but at the same time it's very hard to read and follow. That is caused by both lack of clarity on what it is about and the fact each part is written by a different author.

I both enjoy reading this book and hated how much work I had to do to comprehend what it was about.
