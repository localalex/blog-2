---
title: "Denis E. Taylor - Heaven's River"
date: 2021-11-12
started: 2021-10-31
openlibrary: https://openlibrary.org/books/OL31462977M/Heaven's_River
shelves:
  - fiction
  - sci-fi
---

Read the story of several spaceships that contain uploaded human minds. These spaceships are Von Neumann probes; self-replicating spaceships meant to spread through the galaxy. Each spaceship has a cloned mined of an Earth engineer called Robert (Bob).

This is the fourth book of the Bobiverse series, so if you are new to Bobiverse, you might want to start with the first book: [We Are Legion (We Are Bob)](https://openlibrary.org/works/OL19123425W/We_Are_Legion_%28We_Are_Bob%29_%28Bobiverse%29_%28Volume_1%29). I also strongly recommend the audiobooks narrated by Ray Porter.

Heaven's river is a megastructure: a large tube orbiting a star with a civilization of otters in it! The book is a story of exploring this structure told by many different spaceships (Bobs).

When I read the [Project Hail Mary](/books/andy-weir-project-hail-mary/), it reminded me of the Bobiverse. And I was stoked to find out that Denis Taylor had written the fourth book since I read the series! Heaven's River.

If *self-replicating sentient spaceships* sound like your jam, read the first book. If you've already read the previous three books, I won't be able to stop you from reading this one even if I tried.
