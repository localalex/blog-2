---
title: "Epictetus - Encheiridioin"
date: 2020-10-19
started: 2020-10-17
goodreads: https://www.goodreads.com/book/show/24614.The_Handbook
shelves:
  - non-fiction
  - philosophy
---

> Some things are up to us and some things are not up to us.

The Stoic philosophy is very close to my heart because it teaches an approach to life, similar to Buddhism. Epictetus suggest that you only worry about things that you can change and forget about those you can't. Furthermore, he suggests that if you learn to want the things that are happening as opposed to the things that are not happening, you are going to be always happy.

The 53 rules, which make the actual content of this book take only about 35% of the pages. Instead of the long introduction explaining Stoic philosophy, I'd love the translator to add a brief explanation and examples of each rule. Without any additional information, I was able to understand maybe half of the rules.

One rule that I really enjoyed was:

> Rule 12: If you want to make progress, give up all considerations like these: “If I neglect my property I will have nothing to live on,” “If I do not punish my slave boy he will be bad.” It is better to die of hunger with distress and fear gone than to live upset in the midst of plenty. It is better for the slave boy to be bad than for you to be in a bad state. Begin therefore with little things. A little oil is spilled, little wine is stolen: say, **“This is the price of tranquillity; this is the price of not being upset.”** Nothing comes for free. When you call the slave boy, keep in mind that he is capable of not paying attention, and even if he does pay attention he is capable of not doing any of the things that you want him to. But he is not in such a good position that your being upset or not depends on him. *Emphasis mine*

This is such a healthy way to look at any troubles in life. Did you crash a car? Well let's not stress about it, the crashed car is a *price for tranquillity*.
