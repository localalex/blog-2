---
title: "Jim Paul - What I Learned Losing a Million Dollars"
date: 2021-11-18
started: 2021-10-16
openlibrary: https://openlibrary.org/books/OL1131535M/What_I_learned_losing_a_million_dollars
shelves:
  - non-fiction
  - finance
  - investing
---

First, you'll read Jim's story as a cautionary tale. Then Jim analyses where he made mistakes. Because of this split into two parts, it's easy to read the book. The first part is just like any other novel, although focused on financial markets. The second part is a philosophy/psychology self-help manual for how to be less emotional when investing.

Even though I invest in index funds and not in individual stocks, I found the advice helpful.

## Why should you read it?

You should read it because it's an easy read that will teach you about the psychology of trading in the stock market. The book has got two hundred pages, and you can read the first part where Jim tells his story in one afternoon.

However, don't expect any specific advice to help you make a concrete decision in the market. Jim is specifically washing his hands from concrete plans and only suggests how to think about investing.

## One key lesson

Have a plan! Have a plan before you invest any money. 

You have to decide what loss are you willing to accept, what profit do you expect. You should prepare scenarios that say, "If A happens, I'm going to sell the stock/wait/buy more". And you have to do this **before** you invest. If you write these before investing and have the discipline to stick with the plan, you at least limit the amount of money you'll lose. The upside is up to you and up to your analysis.

The moment you invest, you'll become emotionally attached to the investment. If you don't have the plan, you'll start justifying your investment even if it loses value. ("It's just a tiny glitch, I'm going to wait a bit longer till the market turns.") You'll make it personal, and you'll lose money *eventually*. 

Eventually is the important word. If you invest emotionally, it can work for you for years the same way it worked for Jim, but ultimately, you'll make a mistake, you'll double down to protect your ego, and you'll lose money.
