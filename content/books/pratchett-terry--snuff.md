---
title: "Terry Pratchett - Snuff"
date: 2021-01-29
started: 2021-01-04
openlibrary: https://openlibrary.org/works/OL16123737W/Snuff
shelves:
  - fiction
  - fantasy
  - comedy
---

Reading Snuff was a bittersweet experience. Sweet because by now it's impossible to disappoint me with stories about the Ankh-Morpork city watch. Bitter because this is the last book fo the series. And since Sir Terence David John Pratchett died in 2015, this is it.

The story has a plot framed in a similar way to the Fifth Elephant. Commander Vimes goes to a foreign land, learns the local dynamic and solves a crime. The plot by itself doesn't sound like much fun; it has been done before. But the opposite is true. Half of the enjoyment comes from knowing the characters and predicting what they are going to do. The second half comes from the unparalleled literary style with hilarious metaphors and timeless descriptions of human(oid) character.

This book closes a year-long adventure. I miss you already Ankh-Morpork city watch.
