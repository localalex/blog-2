---
title: "Scott Adams - How to Fail at Almost Everything and Still Win Big: Kind of the Story of My Life"
date: 2016-09-02
started: 2016-09-02
goodreads: https://www.goodreads.com/book/show/17859574-how-to-fail-at-almost-everything-and-still-win-big
shelves:
  - non-fiction
  - self-help
---

This book is by far the best self-help book I've ever read. It is witty, funny and it touches on all aspects of living a good life. When the book was published, lot of the concepts were not well known (at least to me).

The most important takeaways were:

- Getting a coffee to get yourself to exercise is a good deal even if the coffee by itself could be less healthy
- When you are not hungry or tired, stock up on healthy food and snacks. Make eating healthy much easier than eating rubbish
- Make sure that people can give you feedback (e.g. Scott's email on the Dilbert comic)
- Prefer process over goals, make sure you enjoy the process
