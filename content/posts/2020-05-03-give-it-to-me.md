---
title: "Give it to me: A polite request for feedback"
slug: give-it-to-me
date: 2020-05-03
written: 2020-04-26
---
Today we'll have a look at how you can give me feedback for these articles. I'll explain my motivation for writing, why is your opinion important to me and how you can share it. **I guarantee I'm going to read every single comment.**

<!--more-->

## TL;DR

You can give me feedback through comments on Google Doc[^1]. You can go to this document by clicking the "Give Feedback" button at the end of each article.

## Why do I keep a blog

After I started working remotely full-time, I found out that explaining myself in writing is very different from sharing my thoughts in person. And part of this revelation was that I have plenty of space for improvement there. I started writing this blog to give myself motivation and accountability for learning the skill of writing. So the priority of this blog is to share ideas as clearly as possible, even though, of course, I'm trying to make your reading an enjoyable process too.

## How to do this better

After writing articles for a few months I'm confident that I have formed a habit of writing. But can I do it well? I understand every idea I'm sharing here, but do you? Do the other readers understand? Are there any other readers?

These questions nudged me to think of a feedback process. Scott Adams mentions in his book [How to Fail at Almost Everything and Still Win Big](/books/adams-scott-how-to-fail-at-almost-everything/) the reasons why his Dilbert comic strip got so successful was that he included his email in the drawing itself and after that he started receiving feedback from his readers. See [before](https://dilbert.com/strip/1990-01-01) & [after](https://dilbert.com/strip/2001-01-01). [^2]

## What feedback do I want

Any constructive feedback is highly welcomed. As long as it is civil. I would also highly appreciate any suggestions.

> The conclusion is confusing

That's good. It shows me which part I should work on

> The conclusion is sidetracking from the thought introduced at the beginning of the article. The concept of Flumbus should be dropped and instead the conclusion should be driven from the Plumpot section.

Well done, this is amazing! Not only you are telling me what's wrong, but you are helping me by explaining your understanding. Few of these will get you a mention in credits.

## What feedback should you keep to yourself

Feedback that I can't act on, insults, anything that you wouldn't say in my face: I'm 186 cm, 85 kg, sporty, with a bit of martial arts background.

> This sucks.

I can't act on this feedback, it doesn't give me a way to improve.

> You have to be stupid to write this.

Maybe you have a valid point but it's going to be hard for me to find it with this presentation.

> F#!k you, you @#$!

This is clearly violating the last condition (unless you are a 2 meters tall 100 kg boxer).

## Feedback process

I was considering many alternatives starting with simple solutions like a standard comment section under each article to more complex workflows that would include Git and GitLab. Comment section would provide the lowest barrier to entry, but it would support generic feedback and not provide good enough tools for giving specific feedback. The hard-core option using GitLab would disqualify any person who's not familiar with software development.

### Google Docs

The winner is Google Docs. Google made it extremely simple to contribute to its documents and even though commenting on documents is still not a common practice, it is easy to learn and understand. All the following aspects contributed to the decision:

- Encourages giving concrete feedback as opposed to generic *This is great*
- Familiar software - almost everyone has experience either with Google Docs or Microsoft Word
- Free of charge
- Supports anonymous contribution - No login necessary

## Conclusion

If you decide to spend time and effort and tell me about something that could be improved in my writing or in my fact checking, I'll be incredibly grateful. If your feedback is constructive and helps me improve the article (and myself) I'll include you in a "Credits" section at the bottom of the article as a means of showing at least some of my appreciation.

Let's all improve together!

[^1]: If you haven't got much experience with commenting on documents, the [How to Add Comments in Google Docs](https://www.howtogeek.com/397601/how-to-add-comments-in-google-docs/) should help you.
[^2]: In case you are wondering why I didn't include the pictures directly, it's because Scott is asking $35 a pop :)
