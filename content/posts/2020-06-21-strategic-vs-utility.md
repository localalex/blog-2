---
title: "Focus on strategic and outsource utility work"
blockquote: highlight
date: 2020-06-21
keywords: [productivity, software, finance]
slug: strategic-vs-utility
aliases: ["/posts/2020-06-21-strategic-vs-utility/"]
categories: [productivity]
summary: This article explains the concept of utility and strategic software and how it easily translates into everyday life.
---

This article explains the concept of utility and strategic software and how it easily translates into everyday life. Originally, the concept has been used by large enterprises to decide how they should divide their development efforts. Now, it can help you prioritise your side project just as well.

My parents aren’t rich. Growing up, I had to think about money and scarcity. My dad would always prefer building or fixing something himself over getting a tradie to do it. Saving was always more important than investment.

And that’s why I turn every coin twice before spending it. $5 a month for a service? Are you nuts? That’s $60 a year. I'll build it myself instead. Do you want $200 for a kitchen pot that would simplify my cooking? No, thank you. Website analytics for $10 a month? **I'm going to do it myself to save money.**

Martin Fowler, in his article [Utility vs. Strategic - Dichotomy](https://martinfowler.com/bliki/UtilityVsStrategicDichotomy.html) explains the following concept: *Utility software* is your company's office suite and payroll system. *Strategic software* is a critical system providing your company with a competitive advantage. Google's strategic software is its search engine implementation. You should buy utility software as-is and always build your strategic software.

> If you are Google, you must be perfecting your search engine rather than creating your custom payroll system.

This concept is useful for individuals too.

| You are | Strategic activities | Utility activities |
| --- | --- | --- |
| Blog writer | Producing a high quality reading material | Designing blog, maintaining your computer, maintaining a blogging platform |
| Visual artist | Producing drawings, paintings, designs | Configuring and installing your drawing software, social media marketing, printing and selling your artwork |
| Professional athlete | Training, competing | Transportation, nutrition planning, scheduling the competitions, gear design |

Spending time on a task should be backed by answering YES to one of the following questions:

- Do you enjoy doing it **a lot**?
- Does it set you apart from others? If someone else is going to do the task, will it significantly change the outcome?
- *Is the money saved by doing it yourself worth the effort?*

The last point is the trickiest. It's only too easy to see that you are going to save $100, but the [opportunity cost](/posts/opportunity-cost/) might be much higher. Make sure you are saving enough money to make up for not doing an activity that is more fun, more rewarding or more profitable.

I need to get out of this scarcity, do-it-all-yourself mindset. It's good not to spend money carelessly, but it's worse to spend time on things that don't improve the outcome of my work, and they don't bring me joy. But that's easier said than done, even thinking about spending $10 a month on a service that I can implement myself feels uncomfortable.

I'm going to solve this inertia by forking out a fixed amount I have to spend each month on reducing, automating and replacing my *utility activities*. I will spend $100 each month to support this blog and my other side projects. I hypothesise that I'll enjoy working on them more and they will benefit more people. I'm considering spending on services like DigitalOcean credit, Hemingway app, and Fivver designer.

I want to focus only on the few parts of my hobbies that I enjoy and that bring value to other people and me. I will maximise this provided value by not spending time on things that can be cheaply purchased. I'll try to return to this topic in a few months and let you know how this experiment went.
