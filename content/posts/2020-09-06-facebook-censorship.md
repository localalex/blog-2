---
title: "Facebook won't let you talk about this blog"
slug: facebook-censorship
date: 2020-09-06
keywords: [facebook, privacy, censorship, blocking, messenger]
categories: [securityprivacy]
summary: I'm perplexed by the way Facebook decides what you can and can't talk about with your friends.
---

I've heard about social networks censoring content, but I always thought it only affects hate speech or promoting violence. I never expected that my opinions could end up in the same bucket as stirring violence. And I'm perplexed by the way Facebook decides what you can and can't talk about with your friends.

**UPDATE**(2021-02-08): Today I was able to share this blog through Facebook. So the situation described bellow is no longer happening. That doesn't change my sentiment towards Facebook though.

{{% theme-image width="350px" filename="facebook-censorship.svg" %}}

A few weeks back, I was messaging with my ex-colleague and a good friend. We haven't talked to each other since I started writing this blog and so, naturally, I wanted to brag, and I sent him a link.

![Bragging on Messenger](/images/posts/facebook-censorship/messenger.png)

The message didn't send. Tapping for details didn't work[^1]. I thought it has to be a network issue. I opened my laptop and tried to send the message through the Facebook website.

![Bragging on the web](/images/posts/facebook-censorship/web.png)

I couldn't believe my eyes. Facebook won't let me message my friend because it doesn't agree with the content of my message. This private message censorship is both sad and hilarious at the same time.

It's **hilarious** because it proves the point I was making about end-to-end encryption being necessary when I talked about the [Signal messaging app](/posts/signal). It's nobody's business to know what I'm discussing with my friends. Spying on my messages is one thing, not allowing me to send them at all is whole another level.

It's **sad** for several reasons.

First, I don't rely on my site for making a living, but if I did (as so many of you do), then my livelihood would be damaged. There's no way of knowing which of the plethora of vague [Facebook Community Standards](https://www.facebook.com/communitystandards/) are you allegedly breaking.

Second, not all of my friends are on Signal, and so I sometimes have to talk about my site as `viktomas[dot]com`. It hurts every time I type that into a Facebook message.

Third, this happens retrospectively[^4]. If anybody posted or talked about this blog, Facebook went and deleted those messages, rewriting history.

## The dispute process

There is a dispute process that doesn't work. You can send Facebook a note saying that you think they made a mistake, but your message goes straight to [`/dev/null`](https://en.wikipedia.org/wiki/Null_device).

They'll even tell you that they **"won't have a look at your report"**:

![Trying to report the issue](/images/posts/facebook-censorship/report.png)

I understand that a service with 2.7 billion users[^2], can't handle individual requests. But then they should be a bit more transparent about the reasons why their algorithm blocks your site so you can decide whether you let Facebook dictate your content or not. Facebook designed non-transparent censorship, and it doesn't allow users to contest the decisions. Dreamwidth co-owner xb95 says it the best:

> [It's] a bit of a /shrug moment on the subject itself. "Facebook gonna Facebook" I think is approximately how we feel about this.
>
> xb95 [on hackernews](https://news.ycombinator.com/item?id=23958445)

Countless blog posts[^5] recommend the following 3 steps to resolve the issue:

1. Go through all your content and make sure that it complies with the 27 pages long, vague [Facebook Community Standards](https://www.facebook.com/communitystandards/)
2. Use the useless form that I mentioned earlier
3. Sign up for Facebook Ads and contest their ban as a paying customer

I even found advice recommending to change your domain.

## Others having the same issue

Dramwidth got blocked in July 2020[^3]. FolioHD got blocked in July 2020[^4]. When it happened to FolioHD, t
he founder Corry Watilo had to contact his friends in Facebook and start [a small Twitter campaign](https://twitter.com/watilo/status/1282856692083032064/retweets/with_comments) to unblock his domain.

But the majority of the cases ends up without any resolution. Not many people have the privilege of having friends with friends in the Facebook support centre.

## My take away

When I was in my early twenties, still at uni, Facebook banned me without any explanation. I lost tens of connections to acquaintances that I never recovered. I got burnt, and I haven't had a Facebook account for years after that. But eventually, after the services I used (Whatsapp, Instagram) got one by one acquired by Facebook, I gave in. I started another account with a different email so I can talk to people who aren't on Signal or Whatsapp. I wish I could do without it.

Somebody at Facebook really doesn't like me. But the whole situation is funny rather than angering. I don't need Facebook, and even though it would be nice if you could share my articles the way you want, you are not likely too dependent on Facebook if you follow my blog.

I'm going to embrace "Facebook gonna Facebook" and introduce a new subtitle for my blog: "Content so good that Facebook won't let you talk about it."

[^1]: Android Messenger only shows a dialogue saying that you can't reply to this method. I get it, I also often forget to test for the edge case scenarios when I write code.
[^2]: 2.7 billion monthly active users in Q2 2020 according to [statista](https://www.statista.com/statistics/264810/number-of-monthly-active-facebook-users-worldwide/)
[^3]: [Original post](https://andrewducker.dreamwidth.org/3861716.html), [Hackernews thread](https://news.ycombinator.com/item?id=23956640)
[^4]: [Original post](https://watilo.com/facebooks-community-standards-censorship-has-far-reaching-consequences), [Hackernews thread](https://news.ycombinator.com/item?id=23956988)
[^5]: I'm not going to link them, but the Google search term is `facebook community standards block -site:facebook.com`
