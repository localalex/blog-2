---
title: "Remote"
slug: remote
date: 2020-03-15T19:20:00+01:00
keywords: [remote, lifestyle, productivity, software]
categories: [lifestyle]
---

Interesting times, interesting times. Let's not wait till the government orders quarantine. If your work involves predominantly computer, you have to start working remotely. This is how you do it.

Who am I to talk? I'm a software engineer at GitLab, the largest all remote company in the world. In my ten-year career I've worked in teams as small as two and as large as forty people. I've had all collocated team, I've worked with outsourced offshore team from China and now it's all remote.

![Homeoffice - Ljubica Petković](/remote/homeoffice.jpg)

Picture: **Homeoffice** - Ljubica Petković

I'm not going to lie, this article is heavily inspired by GitLab blog article on the same topic (featured on Hacker News) https://about.gitlab.com/blog/2020/03/06/resources-for-companies-embracing-remote-work/

## Remote vs. Co-located

First, there are different levels of remote work. Let's look at examples ordered by the distance team members have from each other. I'm not considering company branches (e.g. Google in Mountain View vs Google in Sydney).

### Classification

1. No work from home policy. Everyone is in the office
2. Team member is sick and takes a home office
3. One or more team members are working remotely full time
4. Everyone works remotely (no two colleagues collocated), there is no central office

The key difference between the last two modes of remote is that the second last creates the "us vs. them" mentality. Information is often located at the main office and that's where the career opportunities are as well. I'm going to be talking about the last mode of remote. It's the most relevant for current world-wide quarantine.

There are a few things that you usually do in the office that you won't be able to do when working from home:

- tap your colleague on the shoulder and ask a question
- shout out a question and get immediate answer
- if you are a manager: control that your direct reports show up for 8 hours
- have a chat over a water cooler or lunch with your colleagues

Only the last one example is (in my opinion) a downgrade from an office. None of the previous actions is necessary (and as we show even optimal) for knowledge workers to do their job.

Communication is one of the most important parts of work and it's format fundamentally changes when you start working remotely. One of the common misconceptions is to think that remote work is almost the same as office work with a bit more "skype" in it.

When you are not collocated in the office you have a unique chance to make processes asynchronous.

| In the office | Remote |
|--|--|
| Meeting to resolve a specific issue | Issue tracker issue with comments and suggestions |
| Planning or status meetings | Video call with clear agenda written up in advance |
| Asking colleague question about internal process | searching handbook/intranet for an answer |
| Water cooler conversation | Scheduled video call catchup |
| You are measured by the time you put in | You are measured by the results you provide |

## Drawbacks of remote working

It's not all sunshine and rainbows. For a seasoned office worker there might be several unpleasant changes to the processes and in some cases even insurmountable difference of the environment.

Let's go from the mildest issues to the largest (at least from my perspective).

The mildest issue with remote work is to have the discipline to work and not be distracted by social media, news, YouTube and like. This is larger issue for some. What helps me a lot is having a separate office in my flat that I use for work.

Connected to the previous issue is work-life balance. It can be easier to stop thinking about work or stop doing work when you leave physical office as opposed to just closing your laptop in your living room. It's important to set yourself "working hours" and not work during your free time. The important thing here is that the working hours don't have to be anything like the standards nine-to-five. You can start your day early, then go to the gym just before lunch, have a long lunch with a colleague, run your errands in early afternoon when the shops and government offices are empty and then start working again in the evening. Whatever works for you is fine.

The change in thinking and processes is a massive issue with remote work. If the process is not clear, you can just sit behind your computer at home and not know what to do. We'll talk about documentation in more detail in the How to section.

The main and largest issue with remote work is the lack of human contact. This is something I still struggle with.

## Benefits of remote working

### From the employee perspective

The freedom to live where you want is the largest benefit. I always tried to live next to my office to reduce commute but now I actually live in the same building as my office. And that can be in the country if I decide. So far I can live in my home town working for one of the best companies in the world. When I travel in my camper van, I can still work.

You can pick remote company that aligns the best with your values, skill set and ambitions.

Depending on when you live right now you might save a significant portion of your day on commuting.

### From the employer perspective

- savings for office space can be invested in sponsoring your employee's home office
- you can find talent everywhere and cheaper because your employees don't have to pay rent for living in the centre of a larger city
- your employees are going to be more efficient when they can work on their own terms
- thanks to more extensive documentation there is not that much know-how leaving the company with an employee

## How To

So after reading the previous sections you are thinking: That sounds great. And it's not like I have any other option anyway.

### Document your work

The most important thing is documentation. You work on an issue? Document what you are planning to do, ask for comments and then document what you are doing. That way you don't have to give in person/video updates, everyone can read in their own time what's happening. We use GitLab issues for that, but any other issue tracker like Trello, Jira or GitHub should be fine. GitLab issues is great for software companies because it's so close to the source code.

### Document your processes

Documenting processes showed to be one of the most valuable thing for GitLab as it scaled from a dozen or so employees to twelve hundred.

We use [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) f. It's got seamless integration with the git repository where our whole handbook is stored:

- handbook - https://about.gitlab.com/handbook/
- the git repo - https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master

### Meeting as a last resort

Surprising amount of discussion and decisions can be done asynchronously through discussion on issues. When you do have to organize a meeting. Have a clear agenda before. At GitLab we use Google Docs because it's so easy to cooperate on them real-time during the meeting.

For video conferencing we use [Zoom.us](https://zoom.us/).

### Stay focused

Procrastination is a much larger issue when you don't have people around. It would feel weird to open YouTube at work, but at home it can't hurt to watch that new video can it?

I strongly recommend using something like a [Pomodoro timer](https://en.wikipedia.org/wiki/Pomodoro_Technique). I'm dividing my day into twenty-five minute units and if I feel that I need to do something other than work (watch a video, read news, make a coffee) I do it outside of these blocks. Example. I read through my emails the first 25 minute block, then I make myself a coffee and read a bit of news, then I work 3 blocks in a row before taking another break. I do 10 - 14 of these blocks a day depending on meetings (which I don't count as units), how much fun the task is and how urgent it is.


### Organize your own space

If possible you should make sure that you've got your own room when no one else will interrupt or distract you from work.

If that is not possible, you have to at least take some quite corner in your flat and put on headphones.

Either way you should set boundaries with the other members of your household to make sure you they know not to interrupt you when you're working. Otherwise your productivity flies out of the window.

This was my introduction. Another great resources are:

- [Sid (CEO of GitLab) with Savannah Peterson talking about remote](https://www.youtube.com/watch?v=GD9M33_z-dM)

- allremote.info