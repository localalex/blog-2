---
title: "A script to analyse 7+ years of my YouTube history"
slug: youtube-usage
date: 2020-05-08T16:33:00+01:00
keywords: [google, youtube, productivity, software]
aliases: ["/posts/total-youtube-watchtime/"]
categories: [techsoftware]
summary: Use Google Takeout to analyse a complete history of all videos you watched.
---

Before deleting my YouTube account, I used Google Takeout to get a complete history of all videos I watched. I used this history to analyse how much time I have spent watching YouTube and what I watched the most. **I open-sourced the process and the scripts as [Total YouTube Watchtime](https://gitlab.com/viktomas/total-youtube-watchtime) so you can do the same.**

![My watchtime since 2013](/images/posts/youtube-usage/chart-screenshot.png)

I decided to delete my YouTube account as a part of wider de-googling. But I had a few uploaded videos that I wanted to get back from YouTube. I've mentioned [before](/posts/losing-google-account/#prepare-for-the-worst) how I appreciate Google allowing you to download all your data through the Takeout.[^2] When I downloaded all my YouTube data, I've noticed an interesting file included. That file was named `watch-history` and it contained a list of all the videos I've ever watched.

Looking through the file I've found that it contained a title, link and time when I watched each video.

Only the video duration was missing so I used YouTube Data API to find out how long each video is. After that I was ready to look into the history of my YouTube watching. The results are not too flattering.

## I spent **a lot** of time watching YouTube

Ok, now to the meat of the article. How long did I spend watching YouTube? **I watched 21,396 videos totalling 2,042.5 hours in 7 years** since starting my account. That is 255 work days (8h), or 85 full days (24h). **45 minutes per day** on average.

In the past, I read statistics like:

> Watching TV was the leisure activity that occupied the most time (2.8 hours per day), accounting for just over half of all leisure time, on average.[^3]

And to be honest, I thought that's a lot of time spent watching TV and believed myself to be more sophisticated. But now I have to get off my high horse and realize I like to watch videos as much as the next person.

### Disclaimer

There always has to be one, doesn't it? Please take my results with a grain of salt for the following reasons:

- YouTube doesn't tell you how much of the video you have watched, so my computation can't differentiate between watching 1 minute of a 60-minute video and watching the whole thing. For that reason I count at most 30 minutes of each video, even when it's longer than that[^4].
- Some of the videos (15% in my case) are no longer available and YouTube won't tell you how long they were. I counted those as 4 minute long.
- I shuffled the months in the chart above (the values are real but assigned to a different month). I could see patterns of behaviour and decided to keep [my privacy](/posts/privacy/).

## Fun with data

The script I wrote plots aggregated watch time by month into a chart. The chart clearly showed patterns in my watch time and I did correlate them with events like holidays, injuries, sabbaticals and so on. For example, I was able to quickly locate my overseas travel.

### My top 10 videos

I was excited when I realized I'll be able to find out which videos I enjoyed the most in the past years. The excitement was a bit reduced when I got my results and realized that I used to listen to music on YouTube before I got my Spotify account. Not surprisingly, the most watched videos are music clips all watched before 2016 when I got Spotify.

- [Rudimental - Waiting All Night ft. Ella Eyre](https://www.youtube.com/watch?v=M97vR2V4vTs) watched 40 times
- [Laurent Wolf - No Stress](https://www.youtube.com/watch?v=bVRnMrl2oj8) watched 29 times
- [Anders Nilsen - Salsa Tequila](https://www.youtube.com/watch?v=77Ms1oCiDH4) watched 29 times
- [Audioslave - Show Me How to Live](https://www.youtube.com/watch?v=vVXIK1xCRpY) watched 27 times
- [The Dead Weather - Blue Blood Blues](https://www.youtube.com/watch?v=Wg8DMqFuip8) watched 27 times
- [Hilltop Hoods - Cosby Sweater](https://www.youtube.com/watch?v=aB16fJpoj-I) watched 24 times
- [skeewiff ft. george clooney - man of constant sorrow](https://www.youtube.com/watch?v=JdNWatH4vNY) watched 24 times
- [Jack White - Love Interruption](https://www.youtube.com/watch?v=iErNRBTPbEc) watched 22 times
- [Swedish House Mafia - One (Your Name)](https://www.youtube.com/watch?v=PkQ5rEJaTmk) watched 20 times
- [Full Despicable Me Theme Song - Pharrell Williams](https://www.youtube.com/watch?v=axbUCR1nKRA) watched 20 times

## <i class="fas fa-history"></i> Total YouTube Watchtime

I had good fun digging through my YouTube history and when I mentioned it to my friends they were interested in doing the same. So I polished the scripts, documented the process and published all my work as open-source - [Total YouTube Watchtime](https://gitlab.com/viktomas/total-youtube-watchtime).

If you've got some programming background, you should be able to replicate what I've done relatively easily with your own history. If you find any issues or come up with improvements, send me a Merge request.

## Conclusion

YouTube is a big portion of my entertainment so I'm not surprised that I've spent on average 45 minutes each day watching it. Not being surprised doesn't mean I would like to keep spending my time this way and indeed my real chart is showing some good decline in watch time this year. I've got a few tips and tricks on how to reduce the appeal of YouTube. I'm going to share them in the next article.

The main activity I'm replacing YouTube with is [reading](/books).

[^2]: [Google Takeout](https://takeout.google.com/)
[^3]: [American Time Use Survey 2018 results](https://www.bls.gov/news.release/atus.nr0.htm)
[^4]: The longest video I've had in my history is [NASA ISS live feed](https://www.youtube.com/watch?v=RtU_mdL2vBM) 534 days long and counting. I was relieved when I found that I didn't spend almost two years extra watching YouTube.
