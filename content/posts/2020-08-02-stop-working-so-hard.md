---
title: "Start working less to produce more, strange approach to prioritisation"
slug: stop-working-so-hard
date: 2020-08-02
keywords: [productivity, prioritisation, stress, gtd, basecamp]
categories: [productivity]
blockquote: highlight
summary: Reactive work can burn you out without providing much value. Prioritization is the cure.
---

I'm going to talk about preventing burn out and getting a better sense of control at your job. I'll explain why I think we feel overwhelmed at work. Lastly, I suggest setting a hard limit on reactive work, solution based on my own experience.

{{% theme-image width="400px" filename="stop-working-so-hard.svg" %}}

First, let's have a look at what it looks like when you are overwhelmed from work. We'll start by dividing work into two categories: Work that someone defined for you and work that you defined for yourself[^1].

The work that was defined for you is **reactive**. You have less control over when and how you do it. Your boss asked you to review a document by tomorrow. You got an email with a complaint from a client. Your deadline is coming.

The work that you defined for yourself is **proactive**. You decided that the best thing for the product is stronger marketing, and you design a strategy. You know that the lathe at your workshop needs maintenance, and you'll do it before it breaks. You document the common pain points in your product so users can quickly find answers to their problems.

For the purpose of this article:

> Being overwhelmed means having a disproportionate amount of work defined for you.

You know your working process needs improvement, but there's no time to stop and think, improve and learn. You entered a vicious cycle.

When you are overwhelmed, it's caused by insufficient prioritisation and possibly lacking delegation if you are in a position to delegate some of your work. I faced this situation on my previous project where there was virtually unlimited (and not prioritised) pile of work that needed to be done. If that wasn't enough, there were user support requests and failing infrastructure. I've noticed that no matter how long I did this reactive work, there wasn't a significant change in the value I produced.

Countless methods explain how to get better at workload management. Some of the most influential books are [David Allen's Getting Things Done](/books/allen-david-getting-things-done/) and [Stephen Covey's The 7 Habits of Highly Effective People](https://www.goodreads.com/book/show/36072.The_7_Habits_of_Highly_Effective_People). Instead of writing a summary of these books, I want to focus on a specific aspect of handling large workloads: Budgeting[^3], which is prioritisation supported by hard time boxing[^2].

My argument is built on the assumption that not all work is as urgent as it seems. I'll explain that limiting the overall time you spend on reactive work won't hurt your business or job as much as you'd think.

## Reducing time for reactive work forces prioritisation

What will happen when you reduce the commitment to the reactive work by a third? Instead of reacting nine hours a day, you'll react six? From my experience: **nothing much**. Users have to wait longer to get your response. Some conversations will fall through the cracks. A handful of people get really upset because you won't have time to fulfil their requests. That's it.

When you do this, you will get a few more unpleasant conversations, but you'll get three hours of your life back every day. Instead of finishing late, you can now finish on time.

Even better, you have **two hours** to work on something that you define for yourself. In these two hours, you can improve the process, improve documentation, automate some part of your work, or just finish early to recharge your batteries. The added benefit is that the proactive work starts in mid-term, reducing your reactive work. You'll enter a virtuous cycle[^4].

Prioritisation is the best productivity method because not doing something saves more energy than doing it efficiently. By limiting the time you spend on reactive work, you force yourself to prioritise more. This is the same approach that Basecamp uses for their software development process[^2].

They limit each project to six weeks. The time limit won't change, only the scope of the project will. When they know they've got only six weeks, it's easier to make the decision about what to include and what to leave out. The same applies when you limit your reactive part of your workday to six hours. Six hours is fixed, the content of those six hours is up to you. Not everything is going to make it, and you have to pick the crucial bits.

I found this method naturally as I was burning out on my previous project. I couldn't do the reactive part of my work for eight-plus hours any more. As I reduced the time, I started by just finishing early. For the first couple of weeks, I worked only those six hours, recharging my batteries. But then, I started having energy and motivation to define some of my own work. I improved the user support process and automated some of my tasks.

I highly recommend trying this if you find yourself reacting to external stimuli the whole day. The sense of control and much-needed rest is going to be worth those few more unpleasant conversations.

[^1]: I borrowed this classification from [Getting Things Done](/books/allen-david-getting-things-done/)
[^2]: [Fried, Hansson - It Doesn't Have to Be Crazy at Work](/books/fried-hansson--it-doesnt-have-to-be-crazy-at-work/)
[^3]: The term budgeting is from the "It Doesn't Have to Be Crazy at Work, and it's described as appetite in the [Shape Up](https://basecamp.com/shapeup/1.2-chapter-03#fixed-time-variable-scope) by Ryan Singer
[^4]: [Virtuous cycle on Wikipedia](https://en.wikipedia.org/wiki/Virtuous_circle_and_vicious_circle)
