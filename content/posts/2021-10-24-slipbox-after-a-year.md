---
title: "Zettelkasten note-taking after one year"
slug: slip-box-after-a-year
date: 2021-10-24
keywords: [productivity, learning, notes]
categories: [productivity]
summary: I've been taking small, permanent, densely-linked notes for over a year. Now I'll share how my understanding of the Zettelkasten (aka Slip-box) method changed and what I've learned. 16 months ago, I wrote a popular post about Zettelkasten right after reading the How to Take Smart Notes book. I've been taking smart notes since then. This is my update.
---
I've been taking small, permanent, densely-linked notes for over a year. Now I'll share how my understanding of the Zettelkasten (aka Slip-box) method changed and what I've learned. 16 months ago, I wrote a [popular post](/posts/slip-box/) about Zettelkasten. I wrote it right after reading the [How to Take Smart Notes](/books/ahrens-sonke-how-to-take-smart-notes/) book. I've been taking smart notes since then. I changed my process and software, but the premise is still the same. This is my update.

{{% theme-image width="100%" filename="slipbox-after-a-year/slipbox-after-a-year.jpg" name="Year of Slip-box" %}}

## Why? (the motivation)

I got hooked on the Slip-box method because it focuses on building a *long-lasting* knowledge base. The premise is that you'll understand something once, and then you'll always have this understanding stored in your Slip-box as a note. Wouldn't that be wonderful?

Before I learned about Slip-box, [I forgot most of the information I read](/posts/reading-is-hard). I would write notes that adopt the book's terminology and structure. Writing notes this way made it impossible to reuse them when I read another book on the topic.

The "why" hasn't changed.

## What? (the method)

Slip-box consists of short, atomic, densely-linked notes optimised for reading.

- **Short** - Short note is easier to read and keep it on a single topic. I try to keep my notes about four paragraphs or less.
- **Atomic** - The note contains one idea or fact, making the notes easier to link and deduplicate.
- **Densely-linked** - When the note uses another thought or fact, you don't repeat it but refer (link) to it.
- ** Optimised for reading** - You spend more time writing and polishing the note because you expect to read it over and over again as opposed to the write-and-forget style.

You optimise these notes for reading. You spent more time writing them because you explain the topic with [low context](https://about.gitlab.com/company/culture/all-remote/effective-communication/#understanding-low-context-communication) to your future self who already forgot all about it.

Should you store a piece of information in your slip-box, or should you put it somewhere else?

Imagine you learn about a new concept:

> e.g. Idea from Yuval Harari's Sapiens that Europe and Asia developed better civilisation than Americas because Americas span vertically lot of climates making it harder to share agriculture progress between different climates.

Surely you want to put it in your Slip-box, but what about a shopping list? Or an idea for your new side-project?

I initially kept two note systems, one for the long-lasting "permanent" notes and another for the day-to-day "ephemeral" notes. But now I keep all my notes in one place. I don't have to decide which app to open to capture a thought. If the note shows not to be useful, I can delete it, or it will naturally fade into the background because it's not densely linked to other notes.

Creating links between notes makes you answer two questions for each new note:

- How does it fit into my existing knowledge?
- Does this piece of information conflict with my previous understanding?

Answering these questions takes *a lot* of time, but it pays off in the quality of your understanding and the ease of reading.

## How? (the software)

Now let's talk about how do I create and store my notes? I'll walk you through the tools I tried (the cool part) and the process I use for taking my notes. 

### The tool

If you are like me, you'll spend a lot of time looking for the perfect tool at the expense of more productive activities like reading and learning. Here you might get lucky. You can start where I finished (Logseq) and be completely content with the solution the same I am now.

Once I understood the Slip-box method, I had a clear idea about the requirements for the tool (ordered by priority):

- **Local plain text** - It had to store the notes locally in plain text (Markdown), so I can version them with `git`. This disqualified cloud-based apps like Evernote, Notion, or even TidlyWiki.
- **Links** - The tool has to support creating links between files as references to other notes.
- **Easy to read** - Reading experience is more important than writing because you'll be reading the note many times in the future.
- **Easy to write** - Creating new notes shouldn't be unnecessarily complex (e.g. I don't want to write a text note and then run a command to render my notes as web page).
- **Open-source** - If I can help it at all, the tool should be open-source. Even if the original author stops maintaining it, the tool has a chance to live on.

#### Dead end 1: Zettlr

I started taking notes with [Zettlr](https://www.zettlr.com/) . It is an open-source note-taking app that heavily relies on WikiLinks ( `[[202103100852]]` ) to create links between files. Zettlr also automatically renders the markdown you are writing. It makes it easier to read the note even though you are constantly in edit mode.

After a month or two, I started longing for a universal format because WikiLinks needed custom software to resolve them ( I came into terms with WikiLinks later). I decided to use plain markdown. It supports linking out of the box, and there are countless available tools to write and process it.

![Zettlr](/images/posts/slipbox-after-a-year/zettlr.png)

#### Dead end 2: VS Code

So I left Zettel for [VS Code](https://code.visualstudio.com). VS Code is a text editor for developers, so using it comes naturally to me. I wasn't the only one going in this direction; there are [FoamBubble](https://foambubble.github.io/) and [Dendron](https://www.dendron.so/) projects that customise VS Code for note-taking. And I'm pretty sure [Obsidian](https://obsidian.md/) does as well, even though they don't share their code.

I developed [an extension](https://marketplace.visualstudio.com/items?itemName=viktomas.slipbox) that simplifies the most common tasks like creating and linking notes. I also use [Markdown Links](https://www.markdownguide.org/basic-syntax#links) to visualise relationships between my notes.

This solution worked well for writing notes. I could stay in a familiar environment and insert/format text seamlessly. But reading non-rendered markdown is not pleasant, especially with attachments like images (You see `![description](/path/to/image)` rather than the image itself).

I noticed that I avoided adding images, renaming notes, splitting a note in two, and other operations. It was time to adopt a more complex tool again.


![VS Code](/images/posts/slipbox-after-a-year/vscode.png)

#### The pinnacle of note-taking: Logseq

I knew about [RoamResearch](https://roamresearch.com/) since I started taking Zettelkasten notes, and I liked it. It did what I needed except for having all notes stored in the cloud.

I spent on average two or more hours a week either working on my extension for the VS Code or looking for a perfect tool. And then I found [Logseq](https://logseq.com/).

Logseq was the missing piece for me. It looks like a RoamResearch clone with bits of inspiration from TiddlyWiki and Org-Mode.  It ticks off all the requirements I mentioned earlier. The UI renders each paragraph the moment you stop editing it. That means all the text is nicely rendered, including images.

You can hover over links to see a pop up with the linked note. You can open notes in the sidebar (press `shift` before clicking) and edit two notes simultaneously. The hierarchical bullet-point structure pleasantly surprised me because it makes it easy to reorder my thoughts and move paragraphs between notes.

The app stores all your notes as Markdown text files on your disk and allows you to publish your notes on the web. You can go to [Logseq web app](https://logseq.github.io/) and try out the app without installing the desktop app.

I had to write a short script to convert my markdown notes into the Logseq format [^1], and since then, I've been the happiest user. That was four months ago. Lastly, if you enjoy the app, please support the development team on [their OpenCollective page](https://opencollective.com/logseq) *(no affiliation)*.

![Logseq](/images/posts/slipbox-after-a-year/logseq.png)

### The process

In the following process, I'll use the term *book* to refer to any information resource (e.g. video, blog article, white paper, and so on)

How I create a note:

1. I start reading a book, and I create new reference notes.
1. I write many sentences that phrase my understanding of the book content.
1. I group related sentences by topic.
1. I create a new Permanent note for each interesting thought (group of sentences).
1. I drag the group sentences in the permanent note.
1. I look for related permanent notes to find where I can link the new note. In this step, I often rewrite or edit existing permanent notes to reflect my new understanding.
1. I write the new permanent note in different words but still capture the main thought.

## Main issue - the time

Since I started with Slip-box, I felt that it takes **too much time** to write good notes. It takes me roughly half an hour to produce one permanent note (including the time for reading). For example, when I read [Software Engineering at Google](/books/winters-manshreck-wright-software-engineering-at-google/) it took me three hours to read a chapter and make five permanent notes. I'm comforting myself that now that I put effort to extract the knowledge and linking it with my existing notes, I'll always have it readily available. But was it worth three hours of my time?

I watched [Christian from Zettelkasten.de taking notes from a book](https://www.youtube.com/playlist?list=PLHwtkAtdkx1IZoI7FZf1X50qyzRnmw_Vy). He's a professional note-taker, and it still took him two hours to take four notes in the first video - it does take forever to make good permanent notes.

My reading is now more productive in the long term because I make notes that are easy to revisit. However, it feels less productive because it takes 4-6 times longer to read a book.

Side note: I incorporated spaced repetition with [Anki](https://apps.ankiweb.net/) into my learning, and I can't help but feel that spaced repetition gives me a higher return on investment. ( [How to write good prompts: using spaced repetition to create understanding](https://andymatuschak.org/prompts/) got me hooked.) Maybe I'll share more about that in one of the future blog posts.

![Slip-box and Spaced repetition meme](/images/posts/slipbox-after-a-year/slipbox-spaced-repetition-meme.jpg)

Taking notes is fun. It's even more fun with Logseq. For me, it stopped being a cool trend on HackerNews and Twitter and turned into a general habit that makes me better. However, taking notes with the Slip-box method takes a long time. For example, I stopped reading Thinking, Fast and Slow from Daniel Kahneman because taking notes took too long, and the book felt too dull. Maybe it would be more interesting if I could read it six times faster[^2].

If you haven't tried Slip-box yet, I recommend you do. I recommend using Logseq, org-roam, or RoamResearch. Make atomic notes, create many links between them and don't try to optimise the system till you have hundreds of notes (I've got roughly 400). Good luck!

[^1]: Logseq uses the note title as the filename. I had to rename links like `2021-10-24-note-title.md` to `Note Title.md` . (Yes, that space hurts my feelings.) And then, I had to change all references from `[Note Title](2021-10-24-note-title.md)` to `[[Note Title]]`.
[^2]: As I said, when I read non-fiction and make Slip-box notes, it takes me 4-6 times longer to read the same amount of text.
