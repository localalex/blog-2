---
title: "CSS Week 2 - Grid and Flexbox"
slug: css-week-2-grid-flexbox
date: 2021-03-22
keywords: [css, engineering, challenge, learning, flexbox, cssgrid]
categories: [techsoftware]
summary: Grid and Flexbox are standards for positioning elements on the site. I've spent a week learning about them and then documented my learning.
---

This is another episode of my CSS learning challenge. If you'd like to know how this all started, you can read the [introduction post](/posts/learning-css-in-two-months-week-0).

Last week was fun. Both the Grid and Flexbox are newer CSS features and remove many pain points with positioning elements on the page. Unlike the [last week's selectors](/posts/learning-css-in-two-months-week-1), I could see a practical use for almost every piece of information that I've learned.

{{% theme-image width="600px" filename="css-week-2-grid-flexbox/flexgrid.jpg" name="Flexbox and Grid" %}}

## What did I know

I knew that `display: flex` often solved my positioning problems. If an element didn't align, I set `display: flex` on the parent element and voilà, problem solved. Apart from knowing that Flexbox is a bit more predictable than `display: block`, I didn't know much.

## What did I learn

Flexbox is a surprisingly old standard. In my mind, Flexbox was still at the cutting edge of browser technology, but Chrome fully supports it since 2012 and Firefox since 2014[^1]. Flexbox was the first CSS feature that provided complete control over box-alignment without hacks. This control has been since lifted into it's [own specification](https://drafts.csswg.org/css-align/), and it's also used by the CSS Grid[^2].

{{<center-image width="400px" file="/images/posts/css-week-2-grid-flexbox/flexbox.jpg">}}
*Flexbox aligns items in one dimension*
{{</center-image>}}

Grid is a more recent standard. Chrome and Firefox fully support it since 2017. Using `gap` instead of `grid-gap` to set the spacing between tracks (items) is supported since 2018. The important thing is that the evergreen browsers fully support the CSS Grid Layout.

Grid is meant for two-dimensional positioning (i.e. you need both rows and columns to align), for example, creating the site layout or splitting your image gallery into two columns. You can use Flexbox for a two-dimensional layout as well (and it has been in Bootstrap 4), but it's main purpose and power is a one-dimensional layout (row or column)[^2]. For example, Flexbox would be suitable for Trello-like columns, where you want to align the cards into columns, but you don't need them aligned in rows.

{{<center-image width="400px" file="/images/posts/css-week-2-grid-flexbox/grid.jpg">}}
*Grid aligns items in two dimensions*
{{</center-image>}}

> - do I only need to control the layout by row or column – use a flexbox[^2]
> - do I need to control the layout by row and column – use a grid[^2]

I also completely removed Bootstrap 4 from this blog and used CSS Grid and Flexbox instead. It wasn't as easy as the online courses and web resources suggested. It took me two afternoons.

### Resources for learning

I've listened to both FrontendMasters courses on the topic: [CSS Grids and Flexbox for Responsive Web Design](https://frontendmasters.com/courses/css-grids-flexbox/) and [CSS In-Depth, v2](https://frontendmasters.com/courses/css-in-depth-v2/). If you don't have access to FrontendMasters don't despair, you can get the same information from MDN, but you'll have to put in more work because you won't get spoon-fed the information from video courses. Follow this [MDN guide on Grid layouts](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout) - it's concise and clear.

- [Basic Concepts of grid layout - CSS: Cascading Style Sheets - MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout) - Easy to digest guide explaining grid layouts.
- [Grid garden](http://cssgridgarden.com/) - Fun game with 28 levels where you position water and poison to water plants and kill weeds.
- FrontendMasters
  - [CSS Grids and Flexbox for Responsive Web Design from Jen Kramer](https://frontendmasters.com/courses/css-grids-flexbox/)
  - [CSS In-Depth, v2 from Estelle Weyl](https://frontendmasters.com/courses/css-in-depth-v2/) - the interactive slides are publicly available at [her GitHub](https://estelle.github.io/)

### References

- [A Complete Guide to Grid - CSS-Tricks](https://css-tricks.com/snippets/css/complete-guide-grid/#fr-unit)
- [A Complete Guide to Flexbox - CSS-Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [A Complete Guide to CSS Grid - Codrops CSS Reference](https://tympanus.net/codrops/css_reference/grid/) - much more extensive than it's CSS-Tricks alternative

## What I didn't learn

What I've learnt this week has been immediately applicable, unlike [last week](/posts/learning-css-in-two-months-week-1). However, I still struggled when I tried to apply the knowledge for styling this blog. I understand the concepts well, and if I have a fixed, FullHD screen, I can create a grid with no problem. But I didn't learn enough about the "responsive" part. Making the design responsive, I still create a few breakpoints and change the grid layout based on the breakpoint. Is that the best? I don't know. I hoped that I wouldn't have to use as many media queries after learning these cool new features.

## My process

One week is a short time to learn two CSS standards, but at the same time, I need to keep this pace to get a complete overview in two months. After I know a bit of everything, I can go back and dive deeper.

Next up: Display, position and box alignment

[^1]: [Caniuse flexbox](https://caniuse.com/?search=flexbox)
[^2]: [Relationship of grid layout to other layout methods - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Relationship_of_Grid_Layout)
