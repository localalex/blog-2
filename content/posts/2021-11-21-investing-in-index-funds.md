---
title: "My thoughts on investing: Index fund is all you need"
slug: investing-in-index-funds
date: 2021-11-21
keywords: [investing, index funds, finance, money]
categories: [lifestyle]
summary: I wish I thought about investing when I was younger. The sooner you start, the better. In this article, I'll explain my thought process about investing in and hopefully, I will help you think about money more efficiently.
---

I wish I thought about investing when I was younger. With a low-risk investment, you start to see your money compounding decades in the future. The sooner you start, the better. In this article, I'll explain my thought process about investing and hopefully, I will help you think about money more efficiently.

It all started when I had some extra savings. The interest on my savings account didn't even cover half of the inflation. So I got motivated to learn how other people save and invest money.

{{% theme-image width="600px" filename="investing-in-index-funds.jpg" name="Saving is done" %}}

In the last two years I've read [Random Walk down Wallstreet](/books/malkiel-burton-random-walk-down-wallstreet/), [Simple path to wealth](/books/collins-jl-the-simple-path-to-wealth/), [What I Learned Losing a Million Dollars](/books/jim-paul-what-i-learned-losing-a-million-dollars/), [Black swan](/books/nassim-taleb-the-black-swan/), and many more articles. The more I read about how financial markets work, the more I felt like the Socrates of financial markets "I know that I know nothing". Nobody knows what is going to happen, yet I'm supposed to invest my money. I'll tell you what I learned.

Before we start, here are two disclaimers: 

- First, **I take no responsibility for your actions.** If you invest your money, you are responsible for any gains or losses. I wish we lived in a world where this disclaimer wasn't necessary.
- Second, I assume you don't have any debt with an interest rate larger than 2% (mortgage is ok) and that you are saving money each month, ideally 20% to 50% of your income. If this is true, let's continue; otherwise, you might want to reduce your expenses and pay off your debts first.

### Motivation 1: not investing = burning money

Parting with your cash is not easy. Money on your account is safe, as safe as anything in the financial world could be, backed by the government (in EU up to 100,000 EUR). Taking this hard-earned cash and sending it to the risky stock market is daunting, and if you don't think it through, you might lose the money very quickly. But having cash on a savings account is the same as burning a portion of your money. Here is an exercise that motivated me[^7] to think about investing:

Let's say that you are doing well and you've got $40,000 in your savings account. Interest on your account is 1% a year. Average index fund return is 7%[^1]. This means you are missing out on 6% a year. If you invested in All-World index fund, you could be getting 7% instead of 1%. That's extra $2,400 a year, $200 a month, $6.50 a day. Imagine every morning you take six bucks and flush them down the toilet. Over the long term, it gets even more significant thanks to compound interest. This calculation should motivate you to read up on investing.

### Motivation 2: Early retirement

I took this tool from the book [Simple path to wealth](/books/collins-jl-the-simple-path-to-wealth/). JL Collins analysed past stock market performance[^5] and he concluded that you can spend 4% of your portfolio each year, and the value of your portfolio shouldn't decrease in the long-term. The portfolio should stay roughly the same, adjusted for inflation.

So here's the tool:

> For every $30,000 saved, you'll get $100/month of retirement money.

The 4% is adjusted for inflation. You'll take out $100 per month this year and $102 the next year if the inflation is 2%.

If you are not living from your portfolio yet, this "retirement money" stays there and compounds.


### What options do you have?

I'm only going to talk about passive investment. If you want to start a business, this is not the guide for you. For passive investment, your options are stock market, real estate, bonds, and [crypto](https://www.youtube.com/watch?v=Ntw7Dvctjb4).

The interest you'll get on bonds is low, but buying bonds might still make sense if you want to retire early. More on that later.

That leaves real estate and the stock market. 

Real estate would require more paperwork, more taxes, maintenance, and maybe I'd have to deal with tenants as well. That doesn't sound like a passive investment to me anymore.

That leaves the stock market. 

What stock should you buy? When I read about picking individual stock (e.g. Tesla, Microsoft), between the authors that I trust there seems to be a consensus that amateurs can't pick the winning stock and even fund managers probably can't. An index fund then seemed like the best possible way to start investing.

### What is an index?

An index is a weighted list of securities. For example, S&P 500 is a list of the 500 largest companies, and each company is represented in the index (has a weight) based on its market capitalisation. Market capitalisation is the number of shares times price per share and it is only one of many ways how to weigh the securities in the index.

To get a better idea, check out this [neat table](https://www.slickcharts.com/sp500) that shows the individual weights of the S&P 500 index. 

### What is an index fund?

An index fund is a pool of money invested into an index.

Indexed funds are tracking an index. If the fund tracks S&P 500, then it should perform the same as the 500 largest US companies combined.

The beauty of index funds is that they reduce the risk connected with an individual stock. For example, if Facebook gets regulated and loses half of its evaluation (fingers crossed), it won't make a large dent in the indexed fund value because Facebook is only a small portion of the overall value (1.98% in S&P 500).

That being said, an index fund won't protect you from a stock market crash. If all stock loses value, your index fund does too.

The simplicity of this solution is so enticing that when you visit a subreddit like [r/eupersonalfinance](https://www.reddit.com/r/eupersonalfinance/) you'll see people's portfolio strategy is often "100% VTI and chill"[^2].

You can invest in index funds either directly (using an investment company like Vanguard) or through Exchange Traded Funds (ETF) which work the same as buying an individual stock. Pick the option best for you based on your country and tax law.

### Why would anyone invest in individual stock?

For a human mind, it's tough to have self-reflection to admit that it doesn't know. It's definitely the case with me.

Many "features" of our brain make us think we can pick an individual stock. The important are:

- Hindsight bias - When you look at the price history, and you think you could have anticipated it. "It's obvious that Tesla was going to grow above $900. The climate change requires us to switch to EVs."
- Illusory superiority - When you think you are better than average at most things you do. "Most individual investors lose money, but not you."

The first individual stock I bought dropped 40% in value in the first month, and a year later, it hasn't recovered. I didn't sell the stock, and every time I look at my portfolio, I see it as a reminder not to bet on an individual stock.

Nassim Taleb says that the Beginner's luck for gamblers is a real thing[^3]. If you weren't lucky when you started betting, you didn't become a gambler. I was lucky to lose on my first individual stock. I quickly found out how little I knew.

### The one big assumption: Stock markets are going up

Investing in index funds[^4] is based on the assumption that stock markets are going up in the long term. By that, I mean that if you look at an index for a long enough time, it will increase in value.

[Historically, this is true](https://www.macrotrends.net/1319/dow-jones-100-year-historical-chart) even with the large economic crises of the past. But it is an assumption that is not guaranteed to be true in the future. On the other hand, I assume that if the stock market permanently crashes, I will have bigger problems than not having a good return on my investment.

### Bonds, the exception to the rule

Stock markets always went up in the past, but when looking at the [DJIA 100 year history](https://www.macrotrends.net/1319/dow-jones-100-year-historical-chart) there were several large depressions in the last 100 years. If your investment horizon is shorter than twenty years, you might consider including bonds in your portfolio to reduce the short-term impact of a market crash. I found these three articles very useful in thinking about bonds:

- [How to build an investment portfolio for Long Term Returns | Bankeronwheels.com](https://www.bankeronwheels.com/long-term-investing-strategies-for-financial-independence/)
- [Are bonds done? - JLCollinsnh](https://jlcollinsnh.com/2021/11/14/are-bonds-done/)
- [How to take advantage of the next market crash. | Bankeronwheels.com](https://www.bankeronwheels.com/how-to-take-advantage-of-a-recession/)

### Patience

An investment that increases the value of your portfolio by roughly 7% per year[^1] requires a lot of patience. It's hard to keep your eyes on the target when you see the crypto and meme stock[^6] craziness going on. Why wait twenty years to be financially independent when you "can achieve" the same result in a month if you guess the next rising cryptocurrency and put your life savings on it?

But whilst assuming that Bitcoin is always going to increase in value or thinking that you can get early on the next GME[^6] is very risky, believing that the stock market will increase in value over the next 20 years is much less so.

Edit (2021-11-22): Thanks [@KevSlashNull](https://twitter.com/KevSlashNull) for clarification on the "Motivation 1" example. I said you are losing $6, but you are missing out on $6.

[^1]: [How to build an investment portfolio for Long Term Returns | Bankeronwheels.com](https://www.bankeronwheels.com/long-term-investing-strategies-for-financial-independence/) look for "World ETF Benchmark Total Return (1988-2020)"
[^2]: VTI is a ticker for Vanguard's all-world index ETF.
[^3]: [Black swan](/books/nassim-taleb-the-black-swan/)
[^4]: I mean indexes copying whole stock market like S&P 500, FTSE All-World or MSCI All-World, not speculative indexes like the Green-energy index
[^5]: He used VTSAX index fund 
[^6]: [Meme stock - Wikipedia](https://en.wikipedia.org/wiki/Meme_stock)
[^7]: I wrote [a longer version of this argument](/posts/opportunity-cost/) last year.
