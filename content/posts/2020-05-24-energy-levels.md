---
title: "Optimize your personal energy"
slug: energy-levels
date: 2020-05-24
blockquote: highlight
keywords: [productivity, lifestyle]
categories: [productivity]
summary: how I optimize my energy to be productive and to enjoy my day without an unnecessary struggle.
---

When I was young, I never paid much attention to how much energy I had at any given moment. I never tried to schedule really demanding activities before my meals or avoid difficult tasks just before going to sleep.

In this article, I want to have a look at what I mean by my personal energy and how I optimize it to be productive and to enjoy my day without an unnecessary struggle.

## Personal energy

In the past, I knew that people are getting tired or they are antsy but I never gave much thought to what activities have an effect on the level of my alertness during a day. I took it as something that is "just" happening and can't be affected.

I only realized that our energy during a day can be affected and worked with when I read about how coffee, exercise and eating light meals helps [Scott Adams](/books/adams-scott-how-to-fail-at-almost-everything/) stay focused and maintain good energy. I was in my mid 20's.

But even then, I didn't pay much attention to it, either because I did have enough energy or because I wasn't aware enough. That changed in the last few years when I really started looking at what can I do to feel more alert, productive and happy.

## Trust but verify

This article contains a few tips on how to keep your energy high, but there are no references to the science behind these concepts. Maintaining a high level of personal energy is an individual endeavour. I'm convinced everyone can benefit from the following tips but the best thing to do is to have a baseline of diet/behaviour and then **experiment** and see what gives and what drains your energy.

## Coffee ☕️

OMG, I love coffee so much.

![Hug my brain](/images/posts/coffee-brain-hug-cup.jpg)

*Source: [wanna-joke.com](https://wanna-joke.com/coffee-lovers-2/)*

For me, coffee is the best way to raise my alertness and make myself amped up for creative work or a workout. It's probably not healthy to consume coffee in large doses[^1], but with moderation, this is the ambrosia[^2] for me. On bad days, I use it to make myself work out. But usually, I just have it in regular intervals to feel the kick and get clarity of mind.

## Food

Food affects my energy the most. Heavy meal (a lot of carbohydrates or meat) or junk food (salty, fatty), can easily end the clear, productive day. Or worse, I can still feel queasy the next day.

My silver bullet solution is to eat a lot of veggies, stray away from large portions of carbohydrates and eat meat with moderation, especially red meat. I know, it's nothing new under the sun. However, have you ever tried to eat like this for more than a week? If you are anything like me 5 years ago, you haven't.

## Exercise

This is very counterintuitive. If you go and spend half an hour to an hour working out, you will have much more energy afterwards than when you started. Surely there is some scientific explanation for it. But it's not really important because you can try it for yourself if you haven't yet. Or ask anyone who regularly works out.

This brings me to the ultimate combo:

> You feel tired -> have a coffee -> go workout -> you can do anything now

## Meditation

Sometimes I feel unfocused and low on energy. I would just like to just watch YouTube for hours. Then I'll do 10-20 minutes of mindfulness meditation and as I finish, my thinking is clear and I'm good to do some cognitively complicated tasks.

## Alcohol

Alcohol is great when you want to party all night. Even though [I'm not drinking any more](/alcohol-abstinence/), I've spent the majority of my adult life practising heavy social drinking and I'm not trying to be on a high horse here. A reasonable dose of hard liquor will make you want to go on even when your sober self would be begging to go to the bed.

There is a price for that. At the very least you'll write the next day off.

## Conclusion

Try to be aware of how you feel during the day. If you feel tired ask yourself why? What did you do in the past few hours that made you feel that way? Experiment with doing activities at a different time of the day. And when it comes to increasing your energy, try coffee and exercise.

[^1]: There is not a consensus (AFAIK) on how bad it is to drink coffee but I assume since it's a drug, it can't be healthy.
[^2]: [Ambrosia](https://en.wikipedia.org/wiki/Ambrosia), the drink of gods
