---
title: "Learning CSS in two months"
slug: learning-css-in-two-months-week-0
date: 2021-03-07
keywords: [css, engineering, advanced beginner, challenge, learning]
categories: [techsoftware]
summary: To improve my shallow knowledge of CSS, I'm going to learn CSS for roughly eight hours each week for eight weeks.
---

**TL;DR:** To improve my shallow knowledge of CSS, I'm going to learn CSS for roughly eight hours each week for eight weeks. Once a week I'll write a report about what I've learnt. In the end, I expect to be able to predict how CSS change affects the page. This article is for engineers interested in improving their CSS skills.

{{% theme-image width="650px" filename="learning-css-in-two-months-week-0/backend-fullstack-frontend.jpg" name="Backend to Frontend" %}}

I want to position an SVG image in line with a text. I'll place `img` and `p` tags in my markup, and both elements align to the top of the container. I open web search[^1], type in "CSS image with text vertical center". Scroll for a good looking StackOverflow answer and copy-paste the CSS snippet. If it doesn't work, I'll repeat the process with a bit different search term.

{{<mermaid>}}
graph LR;
A[search for a solution] --> B[select a good looking answer]
B --> C[Paste the answer in my code]
C --> D["Does it work?"]
D --yes--> E[Profit]
D --no-->A
{{</mermaid>}}

Frontend development has been part of my engineering responsibilities for close to a decade. And the chart above explains my go-to development process. On the Dreyfus model[^2], I got stuck as an advanced beginner. That means I can follow instructions, but I lack an understanding of the underlying principles.

Over the years, I've come up with many excuses for not learning CSS well:

- I'm not artistic. I have a very binary understanding of design: Does it look good or not?
- UI is not an exact science. You can't write unit-tests for how easy it is to use.
- I told myself the HTML/CSS part of software engineering is not as valuable as the backend or backend-of-the-frontend work. I blame it on being surrounded by backend engineers for the first several years of my career.
- The longevity of the frontend code is in orders of magnitude shorter than the backend. UI design and frontend technologies are changing too often.

These excuses have one thing in common, an implicit assumption that it's either/or. Zero sum. If I learn CSS, it will push out what I know about databases. If I learn UI design principles, somehow, it'll incinerate my algorithm knowledge.

{{% theme-image width="450px" filename="learning-css-in-two-months-week-0/backend.jpg" name="Backend" %}}

It's time to test that assumption. I'm finally going to learn about CSS and UI design. And if that pushes some important backend knowledge out of my head, I'll serve as both warning and validation for all the devs who resist learning CSS.

## Benefits of knowing CSS well

But why would I want to change the situation? Shouldn't I stay happily oblivious to the mysteries of fronted craft?

My two primary motivators are a feeling of competence and being able to express my ideas better.

The feeling of competence comes from being able to predict the outcome based on the action. E.g. I'll change the `display` property from `inline` to `block`, and I'll be able to predict the result even before the browser renders it.

I had an improved competence experience recently with TypeScript. I finished [Effective Typescript](/books/vanderkam-dan--effective-typescript/). After reading the book, I understand most of the type error messages, and I can fix them without trial and error or online searching.

CSS is a part of my job, and it's here to stay. I might as well feel competent at it. Learning it will also save me a lot of time by avoiding the trial and error process mentioned earlier.

The second and equally important motivator is to be able to express my thoughts better. Being able to build design prototypes quickly will allow me to sketch out frontends for my ideas. I've been using Bootstrap, but I couldn't customise it without losing too much time.

{{% theme-image width="450px" filename="learning-css-in-two-months-week-0/fullstack.jpg" name="Fullstack" %}}

## The process

My brain works the best in the morning, so I plan on spending the first hour of each workday learning CSS. On the weekends, I'm going to spend three hours each day making [notes](/posts/slip-box/) and summarising what I've learnt into a short article.

## The curriculum

My CSS challenge takes two months. I've already spent a week preparing a plan and writing this article, so I'm going to split the remainder of the two months into eight weeks:

1. [selectors: element, class, id, specificity, pseudo selectors](/posts/learning-css-in-two-months-week-1)
1. [flex, grid, responsive design](/posts/css-week-2-grid-flexbox)
1. [display, position and box alignment](/posts/css-week-3-display-position-box) (plus box-sizing, margin, padding, overflow, max/min-width)
1. inheritance: what properties are getting inherited, how is inheritance used when designing the page, rem, em
1. CSS pre and post processing (SASS, PostCSS)
1. design basics (colors, composition)
1. typography - fonts, how are they loaded, what font combinations should I use, strong, emphasis, webfonts
1. accessibility (this might be a very HTML-oriented topic)

I'm omitting any CSS animation for lack of time.

After finishing my two-months challenge, I will come back and edit the list to be the most accurate for other full-stack devs.

{{% theme-image width="450px" filename="learning-css-in-two-months-week-0/frontend.jpg" name="Frontend" %}}

### Learning materials

[GitLab paid](https://about.gitlab.com/handbook/people-group/learning-and-development/) for my [FrontendMasters](https://frontendmasters.com) account, which is going to be the primary source of overview information. I also considered [Head-First HTML and CSS](https://openlibrary.org/works/OL17549103W/Head_First_Html_And_Css?edition=headfirsthtmlcss00robs), but it might not provide me with information fast enough since it's very beginner-oriented.

I'll use MDN for documentation, and I'll link any other articles/materials relevant to the particular week in the week's report.

## Let's start

Tomorrow is the first day of the rest of my frontend engineering career. I'm incredibly excited about this experiment. Starting with the boxing model, I'm going to take on the CSS world.

[^1]: DuckDuckGo.com
[^2]: [Dreyfus model of skill acquisition - Wikipedia](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition)
