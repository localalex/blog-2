---
title: "How to be less gullible"
slug: how-to-be-less-gullible
date: 2021-02-24
keywords: [trust, truth, critical thinking, scepticism]
categories: [lifestyle]
summary: Why we believe so easily to other people and how to be more critical.
---

I trust people. I trust strangers, news on the internet, and books. 90% times, this is a good strategy, but 10% is still worth considering. This article will explain why people are more likely to trust than to distrust and, more importantly, how we can improve our critical thinking.

{{% theme-image width="450px" filename="how-to-be-less-gullible.svg" %}}

I clearly remember a scene from America's Got Talent. This sketchy magician[^1] introduced himself and said he used to be Marilyn Manson's roommate. I didn't think about it twice; a guy on a TV said it; it's probably true.

After the performance, the mean judge said that Marilyn Manson is a millionaire who doesn't need roommates. It was such an obvious conclusion, and I should have thought of that. I was gullible again!

To me, this happens often. I assume people tell the truth without too much second-guessing. I think it's happening for two reasons, upbringing and genetics.

## Upbringing

When growing up, my dad required obedience and honesty, above all else. The concept of lying wasn't a part of my childhood; the same way swearing isn't part of Christianity. I tried it a few times. Every attempt got severely punished. I ended up thinking, "When lying hurts this much, why would anyone do it".

I'm so far up on the honesty scale that when somebody pretends to throw a stick to a dog, but they hide it behind their back, I see that as lying, especially if they let the dog search for the stick for more than a few seconds.

When I'm this conditioned to honesty, it's easy to assume everyone else is too[^3]. You judge others based on your own experience.

## Genetics

I've read somewhere[^4] that people are, on average, more trusting than not. Historically, people needed to collaborate. Trusting the other people by default was a more effective strategy, even if you got burned a few times. Imagine a group of trusting people fighting over resources with a group of people who don't trust each other.

There is a cost connected to critical thinking. Scepticism makes it harder to take in new information; you have to doubt it first. It also makes you less popular if you often contest what others say.

However, critical thinking and scepticism are now more important than ever. Today your best friend can start a sentence with "I've read on Facebook that..." and you need to have tools to deal with data of varying trustworthiness coming in.

## How to fight the gullibleness

Are you as gullible as I am? Way on the "trusting" side of the spectrum? We will probably never be as sceptical or as critical thinking as we'd like, but we can improve. Here are four ways how I've learnt to be more critical and how I make sure that my knowledge is based on truth as much as possible.

### Put your information on paper

The brain sucks at recognising conflicting information, especially when it receives it at different times. One week you read about carbohydrates being the energy for your brain. The next week you read about a low-carb diet significantly improving cognitive functions. Without putting this on paper, your brain is not likely to recognise the conflict.

Note-taking like [Zettelkasten](/posts/slip-box/) solves this issue by forcing you to integrate the new piece of information with your existing knowledge (notes). If there is a blatant conflict, you need to think about it and resolve it before putting in the new note.

[Zettelkasten](/posts/slip-box/) note-taking is a time-consuming activity, and it's only worth doing for topics you are interested in. For topics that are not too attractive for you, there is the next option.

### Say: I don't know, and I don't care

There is so much information in the world. It's not your obligation to ingest it all and to have an opinion on it. When you hear people talking about the US policy in the middle east, it's OK to zone out, tell them that you don't know anything about it and that you don't care.

You don't have to be an ass about it. You can think the "don't care" part to yourself and politely say that you don't know enough about the topic to comment on it.

If you take only one thing from this article, it should be zoning out when your friends or media start talking about stuff you don't understand. You reduce the risk of storing rubbish in your memory.

**World would be a more beautiful place if people said, "I don't know." more often.**

### Seek debates with opposite opinions

This one is my favourite. Since I don't naturally doubt new information, I can let someone else do it for me. When you watch two smart people arguing for and against a resolution, it's much easier to make up your own opinion.

The best sources of high quality, civil debates and discussions are [Munk Debates](https://munkdebates.com/) and [Comment sections on HackerNews](https://news.ycombinator.com/). The latter is especially close to my heart because most of the participants are engineers and the technical language makes it easy for me to understand and relate.

### Read books

[Reading books is hard](/posts/reading-is-hard/), but compared to other sources of information like YouTube or news, books are backed by more research and have a more wholesome take on a topic.

I'm mentioning reading books as last because it doesn't actively support scepticism like the previous three methods. However, an in-depth understanding of a topic provides you with a better starting position to recognise someone not telling the truth.

These days we need to be more critical about the information that comes our way. We can fight disinformation by making good quality notes, not caring about certain topics, listening to other smart people debating, and reading books on the topics we are interested in.

[^1]: [Rudy Coby - Wikipedia](https://en.wikipedia.org/wiki/Rudy_Coby)
[^3]: I would lie if I insisted that I always speak the truth. No one does. I don't lie too often, and I'm not good at it.
[^4]: For the love of me, I can't find the source. If you know it, please share it at me@viktomas.com
