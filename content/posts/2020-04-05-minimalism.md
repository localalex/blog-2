---
title: "Minimalism"
slug: minimalism
date: 2020-04-05T14:02:00+02:00
keywords: [lifestyle, minimalism]
categories: [lifestyle]
---

I was sitting in my rented room in central Melbourne. After months of preparations I was closing *my suitcase*, expecting an Uber any minute taking me to the airport. Leaving Australia after almost five years of living there and closing or at least pausing a chapter in my life.

This article is going to be focused on one critical section of the previous paragraph: "my suitcase". I'm going to be writing about how I managed to squish five years of my life to a suitcase, even though I lived in a fully furnished house, owned motorbike and at one point in time even a car.

## Minimalism

This term is heavily used in hipster, millennial setting. My definition might be a bit different.

> Minimalism is a process of evaluating your current and future activities and possessions based on benefit to stress ratio. And only accepting the ones where the benefit is significantly larger than stress.

The way people sometimes look at minimalism is as process of throwing away stuff you don't use. That's surely under my definition, since a vase you haven't used for two years doesn't bring you any benefit, but the stress (cleaning it, having space for it, even just having it in your life) is non-zero.

But my definition is a bit broader and can be used on activities or even on things like a car which you are still using. But I'll be focusing mainly on material stuff since it's the easiest to understand. The following table illustrates the process:

| Activity/Possession | Benefit | Stress | Result |
| --- | --- | --- | --- |
| Car when living in city centre | Freedom to go for weekend trips or shopping to a mall | Repairs, registration fees, parking, insurance, .. | Sell |
| Tennis racket when I'm not a tennis player | I don't have to pay the rent when I go once every two years to play | storage space | Sell/Give away |
| Expensive headphones | Listening to good music every day | Lot of money, worrying that I can lose them | Keep |
| Drinking | Easy to be social, meeting more new people, good stories | Costs money, lost days to hungover, the morning shame | Stop |

## Advantages to practising minimalism

By definition, practising minimalism reduces stress in your life. That's by far the biggest reason to use it for me. But it is not the only one.

Minimalism helps you live below your means. If you focus on only keeping/buying the stuff that brings you exceptional value, you don't have to spend all the money you are earning and you can even focus on [using your savings to act on some opportunities](/posts/opportunity-cost).

Having less, rather than more, stuff in your life makes it easier for you to focus on the stuff that matters to you. With the car example, if you decide to sell your car, you can spend the time and resources on practising martial arts, collecting post stamps or anything else that is more important to you than a car.

## Tips and tricks how to practice minimalism

The key is to minimize your attachment to things. Meaning you don't care too much if you loose a thing, break it or loose it. This is not always going to be the case, I love my expensive headphones and I'd cry if anything happened to them, but as a rule of thumb you should be striving to keep the attachment low with the following tricks:

### 1 - Use iteration

[Iteration](/posts/iteration) is the art of starting small, validating that you really want to continue it that direction and then doing another step. In minimalism, classical example is when you want to start a new hobby, get the bare minimum of cheap tools to begin with and then if you still like it in a month, buy a bit more and then a bit more.

Extension of this trick is to only buy expensive thing if you already own the same kind of thing that is cheaper and you use it very often. One of my recent examples is investing $200 on a new Pocketbook e-book reader only after heavily using the cheapest Kindle for two years.

### 2 - Buy second hand stuff if possible

Imagine you are selling at TV that you are not using any more. If you bought a new one, it could have easily cost you $1500 and you are going to be selling it for $400. But if you bought it second hand, there is a good chance you bought it for $600 or less and you are still selling it for $400.

This example illustrates why it's easier to get rid of second hand stuff. On top of that, the definition of minimalism is to have things with a great benefit to you after you considered stress. If `hustle of buying second hand < $900`, you can see how buying second hand is minimalistic.

### 3 - Always prefer more common things

If you are about to buy new car and you have a choice whether to buy factory Kia Rio or some tuned up, painted Holden Ute, always go with the more common, boring thing (Kia here if you were in doubt). You are less likely to be attached to the more common thing and it's in orders of magnitude easier to sell (extension of 2nd trick).

### 4 - Avoid impulse buying

If you don't **need** to buy the thing immediately, don't. Give it a week or rather two. Especially with expenses over $100. The expectation is more often than not better than the real deal and the expectation and excitement dies down over time. After a couple of weeks you might sober up and realize that the thing you wanted that much is not actually going to bring you that much joy.

My recent example was a 3D printer. Early last week I was supper excited to buy $250 3D printer, thinking about all the things I'm going to print. Now, almost a week later I'm not at all convinced I need it.

## Drawbacks of minimalism

Even though I'm a big proponent of minimalism, there are drawbacks. Especially if you are living on a tight budget and you are in a country house, there might be less stress with hoarding than in a city flat and the benefit of not spending a penny more might be more important to you.

Another drawback I can think of is the "I could totally use that thing I got rid of" effect. If you found a vase that you haven't used for two years and you just threw it away, you can bet your hat on having at least two or three situations in the following month when you could totally use that vase. This is just a side effect of loading the vase again to the front of your mind and it is inseparable part of getting rid of old stuff.

-----

So by following the minimalistic process and by trying to live by the tricks I mentioned, I managed to sell my motorbike for only $500 less than for what I bought it three years earlier. I sold my car for almost the same amount. And with the little furniture I had, it was very similar story. In the end, when I was waiting for my flight at Tullamarine airport, I had room to spare in my suitcase.
