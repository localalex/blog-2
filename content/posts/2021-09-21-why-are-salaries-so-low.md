---
title: "Why are salaries so low"
slug: low-salaries
date: 2021-09-21
keywords: [salaries, economics, work]
categories: [lifestyle]
summary: Is there a link between our compensation and how much value your work creates? I think that reward is disconnected from the value provided. 
---

*Is there a link between our compensation and how much value your work creates? I think that reward is disconnected from the value provided.*

{{% theme-image width="600px" filename="why-are-salaries-so-low.jpg" name="Why are salaries so low" %}}

A few months ago, I binged listened through the [How to Get Rich](https://nav.al/rich) from Naval Ravikant. He talks in great detail about the relationship between the work you do, the value the work provides, and the compensation you'll get for it. And as I listened, one question crystalised in my mind.

  > Why are salaries so low?


Suppose your work has a large multiplier between your input and the value provided (i.e. it scales easily) like software, finance, marketing and arts. In that case, your reward could be in orders of magnitude different from occupations that don't scale like carpentry. In software, you write a part of a program that will work for years and keep bringing value to the program's owner (or their customers). But there seems to be a massive gap between the value of the employee's contribution and their compensation. Why is that?

My current working theory is that people don't get compensated for the value they bring to the company as much as for the amount it would cost to get another person to contribute the same way to the company. In other words, if John, with roughly the same skill set, is willing to work for $100,000 a year, then that's what I'm going to get, regardless of my work bringing $500,000 to the company.

I also considered the option that exactly because some work scales so well, it becomes less valuable, but that wouldn't explain the increase in stock value of software companies.

I have surely a very simplified look at the problem, and in the real world, the salary will be a combination of the "competition" and the "value-added" factors. But for the individual contributor's salary, the "competition" part seems much more significant. Also, I'm not suggesting that employees are getting ripped off. They don't deserve the full reward if they don't carry the risks.

## Way to increase the salary

Naval suggest that you can increase your compensation by getting more accountability for your work. You want to be solely responsible for the work you do, including the downsides. This is why VP's get more stock options. They've got more impact on the company.

Ultimately, Naval hits that the end goal is always to start your own company. Only if you own the company you can fully capture the value.

On a smaller scale, you can shift into a field/location where the competition isn't that fierce. 

My thinking on this topic is not clear. I've never studied economy, and this pondering seems probably trivial and obvious to someone with a microeconomy background. I've decided to read the well-recognised textbook from Gregory Mankiw - [Principles of Microeconomics](https://openlibrary.org/books/OL23174415M/Principles_of_microeconomics). If you have a clear understanding of the topic, let me know at me@viktomas.com.
