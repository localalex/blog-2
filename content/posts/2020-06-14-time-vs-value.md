---
title: "Easy-going work doesn't provide much value"
slug: time-vs-value
date: 2020-06-14
blockquote: highlight
keywords: [productivity]
categories: [productivity]
summary: The whole day of work can feel great, but not provide value to the outside world.
---

The whole day of work can feel great, but not provide value to the outside world. This article explains that the time spent at work isn’t necessarily directly related to the value produced.

This is especially true for children of middle-class parents (myself included). Parents who worked nine to five jobs and got paid a fixed salary for a fixed time.

## Predictable tasks feel great

I got reminded of this concept last month at work. Every week I produced a report for [our service](https://gitter.im). This report would contain the number of active users, messages sent between users and a few other metrics. The value of this report is to provide my team with motivation and understanding of the service.

I looked into the database to get these numbers and compile them into a nice Slack message. This took me 15 minutes each week. And I count an additional 15 minutes as the price of context switching[^1]. Every time I've done this report, I felt satisfaction for a job well done.

But then I was about to move teams and I wanted to reduce the load on my colleagues. So I wrote a program that will create this report automatically every week. It took me five hours to write it. And all of a sudden, my half an hour a week seemed like time wasted and all the satisfaction seemed unfounded.

Was I providing value by creating the report manually each week?

> Sometimes you are doing a task because it's easy, predictable and you feel in control.

## Solving a problem the hard way

 As a software engineer, I'm solving puzzles for a living. Each time I'm having a new problem to solve I decide on a solution quite early on and try to follow through.

 This works 90% of the time but sometimes I get stuck. And instead of coming up with different solutions to the problem, I'm pushing through with my, now the favourite, solution. (Imagine a rat in a maze trying the same dead-end over and over again.)

What I don't realize at the moment is that **time spent trying to solve the issue the hard way doesn't provide any value over other solutions**. The best thing is to get up from the keyboard and either talk to someone about the solution or at least give the problem an hour or so to sit.

This helps to switch from the very low-level solution mode into the big picture view.

> Sometimes the best thing to do is forgetting what you've done and starting over.

## Working out of the inbox

When you work by solving one email after another, you have a clear progress indicator, you can easily measure your work and you can even get into a flow state where you forget yourself.

Also, none of these indicators shows whether you provide value to someone. I would guess that the lack of discrimination and prioritization in the work is causing a massive inefficiency in our value delivery.

A big portion of my work is to review my colleague's work in a process called the code review. I get many emails during the day saying: "A change to the system is ready for your review".

Originally, I would jump on these tasks as soon as they hit my inbox. They have all the advantages mentioned before: they are very predictable, easy to measure and give me a hit of endorphins for finishing them.

Now I schedule these review tasks into multiple blocks a day, allowing me to better work with my [energy levels](/posts/energy-levels/). Solving the hardest problems undistracted when my mind is the clearest.

And some emails that don't seem too important or directly concerning my work, I'll leave unattended till they drop from the first page of my inbox.

## Conclusion

Some work feels great because you know what to do. You are in control. You know the results you can expect. **This is when your alarm bells should go off.**

If the work is this easy, feel-good and predictable, it probably means that it doesn't provide as much value to the world as work that would challenge you.

[^1]: [Context switching](https://blog.rescuetime.com/context-switching/) is a term originally used in computer science. It represents the act of changing the computer memory to get the computer ready for a new process (program). This nicely translates to normal work where you have to change the things in your immediate memory to prepare yourself for solving a new problem.
