---
title: "My workflow"
slug: my-workflow
date: 2020-04-12T17:33:00+02:00
keywords: [productivity, software, remote]
categories: [productivity]
summary: How I structure my work to battle procrastination.
---

Since the dawn of time (my school years) I've been battling procrastination. Doing the assignments at the latest possible time was a popular activity in my social circles and I don't remember when/if I actually started working on an assignment the day I got it.

There was one mystical place where procrastination mostly avoided me and that place was the office. With colleagues sitting all around me and working, there doesn't seem to be any alternative other than to work as well. You can still procrastinate by doing a simpler task instead of more complex task, but in this post when I say procrastination, I'm talking about doing something else than work: watching YouTube, reading blogs and news or chatting with friends over some message platform.

Now the problem with this mystical place called the office is that its powers don't extend to [remote work](/posts/remote). If you are in your flat, it's easy to just open the YouTube video or to message your friend how is she doing. And this applies to anyone working remotely, regardless whether it's full time or 2 days a month.

To battle the procrastination and improve focus I started looking around for a workflow that would help me stay focused during remote work.

## My workflow

My work-in-progress approach to focused work consists of two key parts: 25 minute units of uninterrupted work and structured, plain text task list.

### 25 minute units

I borrowed the core idea from [Pomodoro technique](#apendix-1-pomodoro) and enhanced it with additional set of rules.

You split your work into units of 25 minutes of uninterrupted work. No responding to messages, no checking news. You can do that after the 25 minutes is over but not until then. And you try to focus these units on one thing.

And these additional rules are not part of Pomodoro:

- You work 12 units a day
- You try to take a high level view after finishing a unit (if you were bogged down in task details, you stop and ask: "Is there a different way to achieve the same goal?"), end of the unit is a good time to change direction
- You don't have to take the break (but you have to do the high level view exercise)
- Meetings don't count as a unit

#### timer

I started by using [Be focused app](https://setapp.com/apps/be-focused) on my Mac, but after a while I decided to create my own app for two main reasons:

- I want to use free and open-source software as much as possible.
- I didn't have access to my raw data.

The result is [timer](https://gitlab.com/viktomas/timer/) a tiny CLI utility go program that keeps track of your units in an easy to work with JSON file.

### Structured, plain text task list

This is the second ingredient to my focused work. I keep all my tasks with temporary notes in a text file like `2020-04-12.md`. The file has the following life-cycle.

1. At the end of the previous day I created task file for the next day and I put in the tasks I would like to do tomorrow
2. On the day I use the first unit to go through all my email and put in additional tasks like "respond to John", "review Alex's work"
3. I always put the main task of the day first, but I limit the amount of units I'll work on it (max 6)
4. Then I go through the remaining tasks
5. During the day or at the very latest at the end of the day I try to put all my temporary notes on the GitLab issues[^1] so I don't keep any information to myself.
6. I create a draft of the task file for the next day.

The key trick for me was putting the main task first, but making sure I won't spend the whole day working on it ignoring conversations and other important small tasks.

#### Example file

```markdown
# Debug production database issue (max 4 units)✅

- [link to the issue](https://gitlab.com/project/issues/123)
- temporary note

## Review Matthews code✅

- [link to the MR](https://gitlab.com/project/merge_requests/321)

## Prepare agenda for the monthly meeting✅

- get statistics for the last month❌
- mention the production outage✅
- shout out to John✅
```

## Conclusion

So here you have it. This is still work in progress and I would welcome any feedback. The main sticking point for me is that 5-minute breaks between units sometimes stretch to 40 or more minutes and that I feel guilty if I can finish my units quickly (equal to roughly 7 hours of work) and I feel less guilty if I get distracted and I spent 9 hours at the computer.

[^1]: GitLab issues is a system we use for tracking work tasks.
