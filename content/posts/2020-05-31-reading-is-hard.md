---
title: "Why do you forget most of what you read?"
slug: reading-is-hard
date: 2020-05-31
blockquote: highlight
keywords: [productivity, learning, books]
categories: [productivity]
summary: You read a book, you think you've understood it. Then you try to explain the content to your friend and you find out you don't remember much. Why?
---

Reading books takes time and in the end, you'll forget most of what you've read. The ideas are not getting transmitted to your brain and the concepts seem clear only until you try to explain them.

You are not alone. This article explains this phenomenon and provides useful tools to improve information retention and understanding.

Last year, I read more books than any other year, 28. Roughly half of them are non-fiction. Throughout the year I noticed a recurring issue:

When I finished a book, I had a feeling that I perfectly understood the topic. Then I tried to give a gist to my friend and I wasn't able to create two coherent sentences about the ideas in the book that I spent hours reading.

## Why books don't work

If you haven't already, I recommend reading Andy Matuschak's article [Why books don't work](https://andymatuschak.org/books/). My understanding of the problem is heavily influenced by Andy's post. His article has profoundly changed how I view reading non-fiction. It mentions exactly the experience I had when trying to recall what the book was about. The experience of "I think I've got this" turning into "I have no idea what I'm talking about" in a matter of seconds.

> I realized I'm not alone.

The main takeaway is that the cognitive model of a book is not aligned with how humans learn. Books are based on an idea of clear transmission of the concept from the paper to your head. But human brains need much more interaction and analysis before they remember anything.

Whilst Andy is [focusing his energy on researching novel ways of teaching/presenting information](https://numinous.productions/ttft/), I want to offer a few incremental improvements for learning from books.

## Techniques to improve retention/understanding

Since realizing that reading a book doesn't automatically equal remembering and understanding the concepts, I started researching and experimenting with ways to improve my retention. These are learning techniques and I'm not using them when I want to enjoy the book just for the experience of reading.

### Read the summary first

In the book [Pragmatic Thinking] Andy Hunt mentions the [SQ3R] process that helps comprehension when reading. The S and Q at the beginning stand for Survey and Question. That means go through the table of contents and chapter summaries and come up with questions about the material.

I like to take this tip a bit further and check whether the book has got a summary on [getAbstract.com](https://www.getabstract.com/). I read through the table of contents, chapter summaries and getabstract and I try to find concepts that I would like to learn the most. This process also tells me what to expect when I read the book.

### Highlighting

When I'm reading a book, I'm highlighting interesting sentences or paragraphs in the chapter I read. That doesn't require too much additional effort and allows me to get through the chapter relatively quickly. This is important for two reasons:

- Sense of achievement - When I see quick progress, it motivates me to keep going.
- Low entry barrier - If I have to put more effort into the reading (e.g. I'd have more complicated note-taking during the reading itself), it lowers the chance of me picking my book at all.

### Making notes

When I'm finished with the chapter, I take out my paper notebook and go through the highlighted bits to remind myself what the chapter was about. Then I try to sum up the chapter in a couple of A5 pages **using my own words**. This is important because just copying snippets from the book doesn't make me understand the concepts deeply.

### Three paragraph summary

This is the last part of my reading process. I don't consider the book finished until I wrote a short summary. I'm trying to capture the one or two key takeaways I really want to remember. Only when I write the summary and publish it on [this blog](/books), I consider the book finished.

## Feature experiments and conclusion

I'd love to be even more efficient in my reading and learn how to fast-read so I can skim quickly through parts of the text that are not rich with new information.

Improving the way you learn is really exciting because it is one of the investments that pay off every time you learn something in the future. It's similar to investing money, the sooner you learn how to do it well, the larger compound interest you are about to reap.

I hope my tips sparked some thinking and experimentation of your own.

[Pragmatic Thinking]: https://www.goodreads.com/book/show/3063393-pragmatic-thinking-and-learning
[SQ3R]: https://en.wikipedia.org/wiki/SQ3R
