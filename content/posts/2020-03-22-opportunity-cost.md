---
title: "Opportunity cost"
slug: opportunity-cost
date: 2020-03-22T17:20:00+01:00
keywords: [lifestyle, finance]
categories: [lifestyle]
---

Thanks to the virus the economy goes bananas. My initial instinct is to do nothing. I mean financially. Hold on to my savings and wait this situation out. And this initial reflex, hoarding savings, is what nudged me to write about a concept that is still quite foreign to me.

I grew up in a lower middle class family. Both parents educated but not earning too much money. There was always a big emphasis on saving in my upbringing. Nobody in my vicinity was trying to use money to make more money. You earn money with hard work and you spend it on mortgage, car, food and other necessities.

But there is a cost to gathering money on your savings account. And not an obvious one. The cost is called opportunity cost.

You are making decisions every day, from trivial decisions like should I buy five or eight bananas to life altering like should I buy this house the other one. Sometimes you don't even make the decision consciously like when you stay at home and read a book, you might not even thing about it as a decision but there are many alternatives, go and eat out, go for a workout and so on.

Opportunity cost is the difference between the option you've chosen and the option you haven't. The following examples will shine more light into what I mean.

## Simple savings

So you've been saving for a year and was able to save $10,000. You haven't thought about investing it and you keep the money safe in your savings account at 1.8%p.a.

In a year you are going to earn $180 as an interest on your account. That's the option you've chosen (whether consciously or unconsciously). Option A.

Now alternative would be to put the savings in an indexed fund based on an index like S&P 500. This index grew in 2019 by 20% in value. If you put your $10,000 in this fund in January 2019, you could have potentially made roughly $2,000 on this investment.

Now the cost of this missed opportunity in this example is $1,820 that you've paid but maybe didn't even realize that.

*In this instance your safe bet paid off because the index dropped much more than that in the first quarter of 2020.*

## Renovating a flat

You bought a flat and it needs a renovation before you can find a tenant. If you'd let the tradesmen do it would cost you $20,000 just for the labour. This professional renovation would take four months.

You decided to do this work yourself in your free time over next two years. You saved $20,000 right? Not so fast.

If the self-made renovation takes two years, that's twenty months that you could have been getting rent and you didn't. So you saved $20.000 on the labour fees, but potentially lost 20 months of rent from a tenant. If the monthly income from rent is $600, you didn't save $20,000 but $8.000 and you paid with 2 years of your free time.

## Motivation to consider the cost

It's super easy to do nothing. And we might realize that there is a cost connected with doing nothing, but as long as we don't have an exact number in front of our eyes, we don't have the motivation start considering the opportunities.

That's why I propose the following calculation to spark your and mine enthusiasm about doing something rather than nothing with our savings.

Let's assume option A being keeping your savings in a savings account which exactly covers inflation e.g. 1.5% (which is super generous for the majority of the savings accounts).

For option B, let's play it super safe and invest to government bonds at 5% yearly interest.

| interest | savings initially | savings after 10 years | savings after 20 years |
| --- | --- | --- | --- |
| 1.5% | $10,000 | $11,605.41 | $13,468.55 |
| 5% | $10,000 | $16,288.95 | $26,532.98 |

Source: [Compound Interest Calculator](https://www.thecalculatorsite.com/finance/calculators/compoundinterestcalculator.php)[^1]

On this simple example is shown that the opportunity cost of keeping your ten grand in savings account as opposed to low risk investment is over $13,000 in twenty years, $653 a year, close to $2 a day difference. **This difference only grows bigger if you increase your savings**. I hope that motivates you as much as it motivates me.

[^1]: Compound interval: yearly
