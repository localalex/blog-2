---
title: "Iteration"
slug: iteration
date: 2020-02-29T11:15:00+01:00
keywords: [productivity, iteration]
categories: [productivity]
---

Today I'm going to be writing about one concept that is as common in software development as it is counterintuitive. With the added benefit that it translates well into every day life.

Imagine you are about to:

- pick up painting as a new hobby
- change career from salesperson to a carpenter
- start going to the gym

If you are anything like me, you would do actions like:

- buy yourself expensive art equipment and text books
- quit your job and get a workshop
- sign up for the gym and get new gym gear

If these seem like a reasonable steps, you are in for a treat.

## History of software development

Back in the day when you wanted to create a big system (think internet banking), you'd plan everything in advance. You'd document all the requirements from users (send a payment, log in), you'd create tasks for software engineers, testers, designers and so on. And then you would close teams of people into a building for years and let them build the system. After a few years you would usually end up with a system having the following characteristics:

- it doesn't have a lot of functionality users want
- it has a lot of functionality no one needs
- it was more expensive to build than anticipated

And that would be pretty much the best case scenario. Lot of these projects didn't get finished at all [^1]. This a classic example of a Waterfall Model [^2]. The decision-making is based on a wrong assumption that you can know in advance what you want to build/do. You probably noticed that the description of the process is parallel to the actions that I suggested at the beginning. They also assume that you know best what to do upfront.

## Nice to meet you iteration

There's been too many failed software waterfalls and in the 1990s there's been a shift in thinking about software movement called Agile Software Development [^3]. This movement started process that was closely communicating with the customer and delivering small improvements on the software often.

Imagine that you are going to a customer on Monday, asking: "How are you happy with the current version of your internet shop?" The customer would say: "It's OK but I would like the users to be able to buy a new T-Shirt immediately." The customer tells you about the whole workflow they'd prepared for it but you explain to them that the only thing you can do in a week is a "Buy now" button, that will a) add the goods to the shopping cart and b) goes immediately to the check out. It's much more simple. The whole work took 3 hours. Users can use it by Friday and in the end you'll find out that the users and the customer are happy with this small iteration. No more work is necessary. Customer saved money, you saved time and users got the functionality they needed much earlier.

OK, but how does this translate to a world out of software engineering? Well, I'm glad you asked.

## Iteration in private life

| What you want to do | All at once | Iteration |
| ------ | ---- | ---- |
| pick up painting as a new hobby | buy yourself an expensive art equipment and text books | get a pencil No. 2 and a notebook (together for $2) and do a few free online tutorials before buying another equipment |
| change career from salesperson to a carpenter | quit your job and get a workshop | build a one small piece of furniture for your friend in your garage |
| start going to the gym | sign up for the gym and get new gym gear| do 2x20 minutes of exercise at home with your own weight |

You can see how the iteration requires much less investment and allows you to act much sooner? What if you find out you don't like painting? What if you hate working with wood when it's an order from a customer as opposed to a fun project with no pressure? Or you'll hate gym because of the vibe you get there?

All these things can be avoided or their impact minimized if you start with a tiny "trial" first.

**Iteration is all about delivering the smallest possible value as soon as possible**.

## Two way door decision

What is better than a tiny change that required tiny amount of time/resources? It's a tiny change that can be easily cancelled, reverted or discarded. This makes the decision or change even easier to make because there is close to no repercussion for doing so. These decisions are called *Two way door decisions*[^4] and example of one is to decide you are going to pick up painting, not telling anyone and buying only pencil and cheap notebook. What is the cost of cancelling such a decision? Only your time and $2 for the equipment.

One-way door decision on the other hand might be changing your cell-phone plan and operator. The amount of hustle is much larger but on top of that you might have to sign up for a year or two and cancelling your decision might cost you a massive fine.

I came up with the idea for this article because I just used it. I always loved teaching and explaining technical stuff to people and I'd been toying with an idea of becoming a teacher for quite some time now. Few months back I decided I want to get a job as a part-time teacher on my alma mater secondary school specialized in computer science. But couple weeks ago I've realized I didn't take a single step towards that resolution.

After a bit of introspection I realized that I was doing a waterfall, one-way door decision. I would have to spend tremendous amount of effort to get certified to teach in a government institution and I would have to sign up to a year of regular classes at the very least. When I looked at it this way my love for teaching was dwindling.

So how to make this a small iteration, two-way door decision? I'm going to start with developing an afternoon course for software engineers in technology that I know well. The cost of doing this is only the time it will take me to prepare the course. There is no equipment or certification necessary. As soon as I give the lecture, there is nothing preventing me from not doing it ever again.

[^1]: [Wikipedia: Failed software projects](https://en.wikipedia.org/wiki/List_of_failed_and_overbudget_custom_software_projects)
[^2]: [Wikipedia: Waterfall software development model](https://en.wikipedia.org/wiki/Waterfall_model)
[^3]: [Wikipedia: Agile software development](https://en.wikipedia.org/wiki/Agile_software_development)
[^4]: [GitLab Handbook: Two way door decisions](https://about.gitlab.com/handbook/values/#make-two-way-door-decisions)
