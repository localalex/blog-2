---
title: "Break free from the shackles of YouTube"
slug: break-free-from-youtube
date: 2020-05-17
written: 2020-05-10
blockquote: highlight
keywords: [google, youtube, productivity]
categories: [productivity]
summary: Understand how much time you spend on YouTube, disarm the suggestion algorithm and save time and attention.
---

I've watched YouTube for [2,042 hours in the last 7 years](/posts/youtube-usage). Over those years, I've tried to limit my YouTube watching several times. Every couple of years or so I would go cold turkey, but I always relapsed back to excessive watching. Let's see why YouTube has such power over us, and how to limit our usage without stopping completely.

I've listened to The New York Times podcast [Rabbit Hole](https://www.nytimes.com/2020/04/16/podcasts/rabbit-hole-internet-youtube-virus.html). And Guillaume Chaslot, former Google engineer, talks about developing the YouTube recommendation engine. This piece of software knows about every video you've previously watched and uses machine learning to guess what you are most likely to enjoy next. The issue is that the recommendation engine optimizes for the **time spent watching videos**, not for content quality or value to your education/health/well-being.

That is the reason why an innocent YouTube visit to "just watch the latest SpaceX news" ends up with me watching Connor McGregor's UFC Knockout compilation 3 hours later.

Historically, I sometimes did get fed up with how much time I spent on YouTube and I went cold turkey. Installing a site blocker in my browser[^1] removed my access to YouTube. But eventually, I would end up pausing the blocker and sneaking back in. The longest I lasted without any YouTube was maybe two weeks.

Last time I blocked YouTube was mid-April this year. But towards the end of the month, I was again sneaking on YouTube in a private window where the site blocker wasn't on. But then I noticed something:

**When I used YouTube in a private window, without my account data, I spent much less time watching it.**

The reason being that the recommendation engine loses all its power when I'm not logged in. The recommendations go from *"Even I didn't know I wanted to watch this video so bad"* to *"I've seen half of this and the other half doesn't look interesting at all"*.

> **Level 1 (Beginner)**: Install site blocker[^1] and only watch YouTube in a private window.

Now, "turning off" the recommendation engine goes a long way towards reducing the time spent watching YouTube. But there are two more things you can do to decrease negative effects YouTube has on your productivity.

The first is a recommendation from [Deep Work from Cal Newport](books/newport-cal-deep-work/). Cal Newport says that our distractions (like watching videos on the internet) are not that bad in and of themselves. What makes them detrimental to our focus and attention span is that we reach for them any time the going gets tough. Did you get stuck on a task? YouTube! Are you waiting 5 minutes before your friend arrives? HackerNews! And this way we train our brain to give up on a task too easily.

> **Level 2 (Intermediate)**: Never switch to YouTube on a whim. Plan when are you going to catch up on the latest videos.

We can significantly reduce the negative effects distractions have on our focus by scheduling distractions in our day. Saying *I'll watch funny cat videos for twenty minutes starting in an hour*. Doesn't hurt the willpower anywhere near as much as saying *Writing this article is boring, what's happening on YouTube NOW?*

> **Level 3 (Expert)**: Burn the bridges. [Delete your YouTube account](https://myaccount.google.com/deleteservices)

A week ago, I took the first advice from this article to the extreme. I [backed up my watching history and did some fun analysis on it](/posts/youtube-usage) but then I deleted all the YouTube data in my Google account. Since then, I don't even need a site block + private window combination. I get the same effect, but this time around I can't go back[^2].

In this article, we looked at what I think is the most addictive component of YouTube, the recommendation engine. Then I suggested several ways to reduce the negative impact this recommendation engine has on your productivity, ending with a hardcore *"delete all your history"* advice. I followed my advice and likely permanently reduced my YouTube dependence.

[^1]: [LeechBlock](https://www.proginosko.com/leechblock/) is my favourite lately. I was using [Blocksite](https://addons.mozilla.org/en-US/firefox/addon/blocksite/) in the past, but it was routinely using so much CPU that I could fry eggs on my MacBook.
[^2]: YouTube doesn't support importing your watch history. Once you delete your account data, you won't be getting as good recommendations as you used to. Not for the next few years.
