---
title: "Designing a digital pirate map"
slug: digital-pirate-map
date: 2020-07-25
keywords: [password, maps, pirates, privacy, security]
categories: [securityprivacy]
summary: Thought exercise exploring a way of storing secrets on the internet.
---

When I was little, I loved pirate tales. There was always a great deal of adventure and exotic lands. And one more thing was almost given: There is going to be a treasure, and someone is going to have the map. Without the map, you don't get the treasure.

{{% theme-image width="380px" filename="digital-pirate-map.svg" %}}

Today we are going to design a digital pirate map. A service where you can hide a text secret (treasure) and only you can ever find it.

When I wrote about [losing your Google account](/posts/losing-google-account/) I thought about where I could hide the recovery keys for my services. My idea was to put them in my parent's house or put them in a time capsule and hide them in my backyard. We will do better than that.

## How will it look

Imagine a website like Google Maps, just more piraty.

### Saving secrets

1. Enter your date of birth
1. Click on a map
1. Store your secret

### Reading secrets

1. Enter your date of birth
1. Click on a map (you need to click within 5m radius)
1. See your secret

This service would be used for secrets recovery and time capsule type of information so we can't ask people to enter a password. That would be like looking for a torch, at night, without a torch. However, we can't just let users click on the map and show them the secret because someone might create a meter by meter grid and ask for all passwords in an area.

We need information about the user that they'll never forget, but it won't identify them too closely. The second property is going to be useful if someone hacks the service. We are going to use **the date of birth**. Knowing that someone born on the second of September 1974 stored `youllneverguessmypassword` secret at the foot of the Empire State Building in New York is not as useful to the hacker as knowing that it was Timothy Hammerworth.

## Encryption

The secretes must be encrypted so they can't get compromised if/when someone hacks the server. Requiring the user to remember a strong password would defeat the purpose. We want people to get their secrets just by knowing the location on the map.

### Naive solution that won't scale

When you click on a map, the click gets translated to GPS coordinates like [(`48.858303`, `2.294414`)](https://www.google.com/maps/place/48°51'29.9"N+2°17'39.9"E/@48.8583039,2.2938668,19z). This coordinate is then used as a password and encrypts your secret. Later, when the service wants to find out whether there is a secret under a given coordinate, it needs to try to decrypt every secret on the server.

This wouldn't scale. Even if one decryption attempt took only 10 ms, with a thousand secrets, we would have to wait ten seconds for each response. You click, you wait ten seconds, you get an answer. All the while, hoping there is no one else using the service.

We will improve on this solution by categorising these encrypted secrets by user's date of birth. So when someone tries to retrieve a secret, we only need to decrypt all the secrets stored for their date of birth. This speeds up the process by several orders of magnitude, but it still starts to break down if there are more than 100 secrets for the same date of birth.

## General issues

### Map precision

If we set the required precision to 5m, then we are strongly dependent on the map visual accuracy. I'm not sure whether the GPS coordinates always land on the same visual representation on a map, e.g. corner of a house. How would Google Maps compare with Open Street Maps? Would there be a strong lock-in with one provider? If this provider updates their maps, are people going to lose their secrets?

### Rounding up coordinates

The service can't allow just any coordinates. That would mean that you have to click with centimetre accuracy. We would need to round the coordinate numbers. Every click in a 5m x 5m square needs to produce the exact same coordinates used as an encryption password. Visually this could be solved by having 5m x 5m grid tiles on the map. This would be more complicated, the world is not a square that we could fill with 5m x 5m squares.

## Are we going to build it?

This idea got me excited and reminded me of my childhood where I was hiding things in the backyard and imagined that those are pirate treasures.

Decrypting a larger number of secrets is going to be demanding on computation resources and hence expensive. There could be potentially some subscription fee. *Pay $10 to save a secret.* But the service would first have to run for years to have the level of trust required for such payment.

Unless we could make the encryption and decryption process extremely cheap, I wouldn't want to build this cool thing.
