---
title: "Slow endorphins"
slug: slow-endorphins
date: 2020-01-22T21:56:01+01:00
keywords: [productivity, lifestyle]
categories: [lifestyle]
---
Watching YouTube or reading a book? Scrolling through Instagram or calling a friend? Eating a pizza or working out? Playing computer games or going out for a walk?

If you are anything like me, you'd much more prefer to do the former. It's so easy that you could almost say those things are doing you and not the other way around. I think this is happening because these things reward you immediately. Your nervous system that is still made much more for the African savannah as opposed to the modern city is incetivizing you to do things that feel good now, there might not be tomorrow.

Two minute YouTube video gives you as much emotion as half an hour of reading a book? Go for it. You can overview all of your friend's lives on Instagram in a fraction of time it would take to have a real conversation with one. And that cheese ❤️.

Modern life is optimized for quick gratification and there might be a good reason to opt out from the viscous loop of fast endorphins. Let's do a bit thought exercise together.

Close your eyes and think about a moment in the past where you were really happy. You spent amazing time with your friends, you achieved something great, you saw something you won't forget for years. After you vividly imagine one scene, try it for two more times. This should give you a sample of 3 worth while experiences.

Now let's see my examples:

1. Spending evening in the back of my camper van with my girlfriend and my best friends
2. Pushing myself to close that stock account I was postponing for months
3. Finishing my last book

Your are probably very different but I assume they'll have one of two (or both) things in common, they either contain people really close to you or they are a result of prolonged effort. The second kind is the one I'm mainly interested in this article.

The more I think about the things I do in life the more short term, high endorphins experiences I'm weeding out of my day to day life. I think that people have been happy for thousands of year without the need for social networks or streaming video services. All I'm asking is you to do your own introspection. Do it in the morning when your mind is really sharp, possibly after your first coffee if you drink one. And see for yourself whether the social media or the video streaming services has a positive effect on your life.

There is another way. You can limit the amount of time you spent on an app each day. Android 10 has a *Digital Wellbeing* section in settings where you can limit the amount of time that an app can be open. And at the same time you can see stats in there that will give you and idea about your current usage. Try to set a limit to 80% of your current average usage. Or 60%. And after a few weeks let me know how did it go?
