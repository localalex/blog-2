FROM debian:10-slim

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /blog

RUN apt-get update
RUN apt install -y libsass1 nodejs npm wget
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.91.2/hugo_extended_0.91.2_Linux-64bit.deb \
    && dpkg -i hugo_extended_0.91.2_Linux-64bit.deb \
    && rm hugo_extended_0.91.2_Linux-64bit.deb
