![Profile picture](/images/avatar.jpg)

Hey, welcome. I'm Tomas. I like software, learning, privacy and healthy lifestyle. I work as a software engineer [@GitLab](https://about.gitlab.com/).

Send me your thoughts: [me@viktomas.com](mailto:me@viktomas.com?subject=Blog%20feedback)
