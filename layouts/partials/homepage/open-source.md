#### Open-Source projects

- [**godu**](https://github.com/viktomas/godu/) - make space on your SSD
- [**Simpleton Analytics**](https://gitlab.com/viktomas/simpleton-analytics) - Analytics made simple
- [**Total YouTube Watchtime**](https://gitlab.com/viktomas/total-youtube-watchtime)
- [**timer**](https://gitlab.com/viktomas/timer) - Measure your work
- [**canonical**](https://gitlab.com/viktomas/timer) - Remove duplicate files
- [**slipbox**](https://gitlab.com/viktomas/slipbox) - VS Code extension for making smarter notes
