<div class="center">
    <img style="max-width: {{ .Get "width" }}" src="/images/posts/{{ .Get "filename" }}" alt="{{ .Get "name" | default "Theme illustration"}}" />

*{{ .Get "name" | default "Illustration" }} by [Ljubica Petkovic](https://ljubicapetkovic.com)*
</div>
